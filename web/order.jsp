<%-- 
    Document   : showProduct
    Created on : Mar 10, 2018, 10:19:50 PM
    Author     : khai
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<c:set var="xmlPage" value="${applicationScope.INFO}"/>
<c:set var="xslPage" value="${applicationScope.NAVLEFT_XSL}"/>
<html class="js no-touchevents" lang="" style="">

    <head>
        <title>Order Page</title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="stylesheet" href="asset/Untitled-2.css" type="text/css">
        <link rel="stylesheet" href="asset/order.css" type="text/css">

        <link href="asset/fontRoboto.css" rel="stylesheet">
        <style type="text/css">
            .fancybox-margin {
                margin-right: 17px;
            }
        </style>
        <script src="asset/showProduct.js" ></script>
    </head>
    <c:set var="ORDERLIST" value="${sessionScope.ORDERLIST}"/>
    <c:if test="${not empty ORDERLIST}">
        yes
    </c:if>
    <c:if test="${empty ORDERLIST}">
        no
    </c:if>
    <body class="tiki-account chrome">
        <header id="header" class="nav-collapse flash-sale">
            <div class="header-form-container">
                <div class="container">
                    <a href="/Assigment1/InitStoreServlet" class="logo">
                        <img src="asset/shopping-cart.svg" width="50px"/>
                    </a>
                    <div class="form-search"  >
                        <form id="search_form" action="" method="get">
                            <div class="search-wrap">
                                <div class="input">
                                    <div class="flex">
                                        <input type="text" name="q" autocomplete="off" onkeyup="typewatch(function () {
                                                    searchSuggestion();
                                                }, 1000);"/>
                                    </div>
                                </div>
                                <button type="submit">
                                    <i class="tikicon icon-search"></i>
                                    <span>Tìm kiếm</span>
                                </button>
                            </div>
                        </form>
                        <div id="search-suggestion" style="display: none" onfocusout="outFocusSearch()">
                            <div data-reactroot="" class="search-suggestion" style="display: none;">
                            </div>
                        </div>
                        <div id="search-autocomplete" style="display: none">
                            <div data-reactroot="" class="keyword-remember" style="display: none;"></div>
                        </div>
                    </div>
                    <div class="header-link">
                        <div class="noti-item tracking-noti item" id="header-noti">
                            <div data-reactroot="">

                            </div>
                        </div>
                        <div class="user-profile item" id="header-user" >
                            <div data-reactroot="" onmouseover="showUserNav(this)" onmouseout="hideUserNav(this)">
                                <div>
                                    <i class="ico ico-user"></i>
                                    <c:if test="${not empty sessionScope.FULLNAME}">
                                        <b><c:out value="${sessionScope.FULLNAME}"/></b>
                                    </c:if>
                                    <c:if test="${empty sessionScope.FULLNAME}">
                                        <b>Đăng nhập</b>
                                    </c:if>
                                    <br>
                                    <small>Tài khoản</small>
                                </div>
                                <div class="box">
                                    <ul class="user-ajax-guest">
                                        <li id="login_link">
                                            <a href="#" class="user-name-login">
                                                <span class="text">Xem thông tin đơn hàng</span>
                                            </a>
                                        </li>
                                        <li class="user-name-register">
                                            <a href="LogoutServlet" title="Đăng xuất">
                                                <span class="text">Đăng xuất</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="header-cart" style="cursor: pointer">
                            <a data-reactroot="" rel="nofollow" href="CartServlet" class="header-cart item">
                                <i class="ico ico-cart"></i>
                                <!-- react-text: 3 -->Giỏ hàng
                                <!-- /react-text -->
                                <span class="cart-count">0</span>
                                <div class="add-to-cart-success">
                                    <span class="close">
                                        <i class="tikicon icon-circle-close"></i>
                                    </span>
                                    <p class="text">
                                        <i class="tikicon icon-circle-tick"></i>
                                        <!-- react-text: 10 -->Thêm vào giỏ hàng thành công!
                                        <!-- /react-text -->
                                    </p>
                                    <button class="btn" onclick="redirectToCart()">Xem giỏ hàng và thanh toán</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-nav">
                <div class="container">

                    <nav class="main-nav-wrap">
                        <a href="javascript:" class="main-nav-toggle">
                            <i class="tikicon icon-burger-menu"></i>
                            <span class="long">DANH MỤC SẢN PHẨM</span>
                            <span class="short">DANH MỤC</span>
                        </a>
                        <ul>

                            <c:if test="${not empty xmlPage}" >
                                <c:if test="${not empty xslPage}" >
                                    <x:transform doc="${xmlPage}" xslt="${xslPage}" />
                                </c:if>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </div>
            <div id="main-ajax-recentlyviewed" data-impress-list-title="Header | Sản phẩm bạn đã xem">
                <div data-reactroot="" class="product-recently-content swiper-carousel-wrapper"></div>
            </div>
        </header>
        <!-- login box -->
        <div class="modal" id="login-form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="hideOnClickOutside(this)">
                        <i class="tiki-icons tiki-icons-close"></i>
                    </button>
                    <div class="modal-body">
                        <div class="content-left">
                            <h2>Đăng nhập</h2>
                            <p>Đăng nhập để theo dõi đơn hàng, lưu
                                <br>danh sách sản phẩm yêu thích, nhận
                                <br> nhiều ưu đãi hấp dẫn.</p>

                        </div>
                        <div class="content-right">
                            <div class="tab">
                                <a class="tab-item active">
                                    Đăng nhập
                                </a>
                                <a class="tab-item" id="create-account" href="#" data-dismiss="modal" data-toggle="modal" data-target="#register-form">
                                    Tạo tài khoản
                                </a>
                            </div>
                            <form class="content bv-form" method="POST" action="https://tiki.vn/customer/account/ajaxLogin?ref=https%253A%252F%252Ftiki.vn%252Ftai-nghe-nhac%252Fc1804%253Fsrc%253Dmega-menu"
                                  id="login_popup_form" novalidate="novalidate">
                                <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="form-group popup_email has-feedback" id="popup_login">
                                    <label class="control-label">Email / SĐT</label>
                                    <input id="popup-login-email" type="text" class="form-control login" name="email" placeholder="Nhập Email hoặc Số điện thoại"
                                           data-bv-field="email">
                                    <i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
                                    <span class="help-block ajax-message"></span>
                                    <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập Email hoặc Số điện thoại</small>
                                    <small class="help-block" data-bv-validator="regexp"
                                           data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Email hoặc Số điện thoại không hợp lệ</small>
                                </div>
                                <div class="form-group popup_password has-feedback" id="popup_password">
                                    <label class="control-label">Mật khẩu</label>
                                    <input type="password" id="login_password" class="form-control login" name="password" placeholder="Mật khẩu từ 6 đến 32 ký tự"
                                           autocomplete="off" data-bv-field="password">
                                    <i class="form-control-feedback" data-bv-icon-for="password" style="display: none;"></i>
                                    <span class="help-block ajax-message"></span>
                                    <small class="help-block" data-bv-validator="stringLength" data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Mật khẩu phải dài từ 6 đến 32 ký tự</small>
                                </div>
                                <div class="login-ajax-captcha" style="display:none">
                                    <div id="login-popup-recaptcha"></div>
                                </div>
                                <div class="form-group" id="error_captcha" style="margin-bottom: 15px">
                                    <span class="help-block ajax-message"></span>
                                </div>
                                <div class="form-group">
                                    <p class="reset">Quên mật khẩu? Nhấn vào
                                        <a data-toggle="modal" data-target="#reset-password-form"
                                           href="#">đây</a>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="login_popup_submit" class="btn btn-info btn-block">Đăng nhập</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end login box -->
        <!-- reset password -->
        <div class="modal" id="reset-password-form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tiki-icons tiki-icons-close"></i>
                        </button>
                        <div class="head">
                            <h2>Quên mật khẩu?</h2>
                            <p>
                                <span>Vui lòng gửi email. Chúng tôi sẽ gửi link khởi tạo mật khẩu mới qua email của bạn.</span>
                            </p>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="https://tiki.vn/customer/account/ajaxForgotPassword" class="content" id="reset_popup_form">
                            <div id="forgot_successful">
                                <span></span>
                            </div>
                            <div class="form-group" id="forgot_pass">
                                <input type="text" name="email" id="email-forgot" class="form-control" value="" required="required" placeholder="Nhập email">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group last">
                                <button type="button" id="reset_form_submit" class="btn btn-info">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end reset password -->
        <!-- registration box -->
        <div class="modal" id="register-form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="tiki-icons tiki-icons-close"></i>
                    </button>
                    <div class="modal-body">
                        <div class="content-left">
                            <h2>Tạo tài khoản</h2>
                            <p>Tạo tài khoản để theo dõi đơn hàng, lưu
                                <br> danh sách sản phẩm yêu thích, nhận
                                <br>nhiều ưu đãi hấp dẫn.</p>
                            <img src="https://tiki.vn/desktop/img/graphic-map.png">
                        </div>
                        <div class="content-right">
                            <div class="tab">
                                <a class="tab-item" data-toggle="modal" data-target="#login-form">
                                    Đăng nhập
                                </a>
                                <a class="tab-item active" id="create-account" href="#" data-dismiss="modal" data-toggle="modal" data-target="#register-form">
                                    Tạo tài khoản
                                </a>
                            </div>
                            <form class="content bv-form" method="POST" action="https://tiki.vn/customer/account/ajaxCreate" id="register_popup_form"
                                  novalidate="novalidate">
                                <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="left-col">
                                    <div form-group="" id="general_error">
                                        <span></span>
                                    </div>
                                    <div class="form-group" id="register_name">
                                        <label class="control-label" for="pasword">Họ tên:</label>
                                        <div class="input-wrap has-feedback">
                                            <input type="text" class="form-control register" name="full_name" id="name" placeholder="Nhập họ tên" data-bv-field="full_name">
                                            <i class="form-control-feedback bv-no-label" data-bv-icon-for="full_name"
                                               style="display: none;"></i>
                                            <span class="help-block ajax-message"></span>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="full_name" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập họ tên</small>
                                        </div>
                                    </div>
                                    <div class="form-group" id="register_email">
                                        <label class="control-label" for="email">Email / SĐT:</label>
                                        <div class="input-wrap has-feedback">
                                            <input type="text" class="form-control register register-email-input" name="email" id="email" placeholder="Nhập Email hoặc Số điện thoại"
                                                   data-bv-field="email">
                                            <i class="form-control-feedback bv-no-label" data-bv-icon-for="email" style="display: none;"></i>
                                            <span class="help-block ajax-message"></span>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập Email Hoặc Số điện thoại</small>
                                            <small class="help-block" data-bv-validator="remote"
                                                   data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Email hoặc Số điện thoại đã tồn tại</small>
                                        </div>
                                    </div>
                                    <div class="form-group" id="register_password">
                                        <label class="control-label" for="pasword">Mật khẩu:</label>
                                        <div class="input-wrap has-feedback">
                                            <input type="password" class="form-control register" name="password" id="password" placeholder="Mật khẩu từ 6 đến 32 ký tự"
                                                   autocomplete="off" data-bv-field="password">
                                            <i class="form-control-feedback bv-no-label" data-bv-icon-for="password"
                                               style="display: none;"></i>
                                            <span class="help-block ajax-message"></span>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập mật khẩu</small>
                                            <small class="help-block" data-bv-validator="stringLength"
                                                   data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Mật khẩu phải dài từ 6 đến 32 ký tự</small>
                                        </div>
                                    </div>
                                    <div class="form-group gender-select-wrap" id="register_name">
                                        <label class="control-label" for="pasword">Giới tính:</label>
                                        <div class="input-wrap has-feedback">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label for="male" class="icheck-wrap gender-select">
                                                        <div class="iradio_square-blue" style="position: relative;">
                                                            <input type="radio" name="gender" value="on" id="male" class="gender"
                                                                   data-bv-field="gender" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        Nam
                                                    </label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="female" class="icheck-wrap gender-select">
                                                        <div class="iradio_square-blue" style="position: relative;">
                                                            <input type="radio" name="gender" value="off" id="female" class="gender"
                                                                   data-bv-field="gender" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i class="form-control-feedback" data-bv-icon-for="gender" style="display: none;"></i>
                                                        Nữ
                                                    </label>
                                                    <small class="help-block" data-bv-validator="notEmpty" data-bv-for="gender" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng chọn giới tính</small>
                                                </div>
                                            </div>
                                            <span class="help-block">Vui lòng chọn giới tính</span>
                                        </div>
                                    </div>
                                    <div class="form-group" id="register_birthday">
                                        <label class="control-label no-lh" for="birthdate">
                                            Ngày Sinh:
                                        </label>
                                        <div class="input-wrap">
                                            <div id="birthday-picker-popup"></div>
                                            <span class="help-block ajax-message" id="span-birthday"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-wrap">
                                            <label for="subcribe" class="icheck-wrap is-small">
                                                <div class="icheckbox_square-blue checked">
                                                    <input type="checkbox" name="newsletter" class="icheck" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                    <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                </div>
                                                Nhận các thông tin và chương trình khuyến mãi của Tiki qua email.
                                            </label>
                                            <div class="input-wrap">
                                                <p class="policy">Khi bạn nhấn Đăng ký, bạn đã đồng ý thực hiện mọi giao dịch mua bán theo
                                                    <a target="_blank" href="http://hotro.tiki.vn/hc/vi/articles/201971214">điều kiện sử dụng và chính sách của Tiki</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group last">
                                        <div class="input-wrap">
                                            <button type="submit" id="register_popup_submit" class="btn btn-info btn-block btn-register-submit">Tạo tài khoản</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end registration box -->
        <div class="breadcrumb-wrap">
            <div class="container">
                <ul class="breadcrumb">
                    <li>
                        <a href="/Assigment1/InitStoreServlet">Trang chủ</a>
                    </li>
                    <!--                    <li>
                                            <a href="/thiet-bi-kts-phu-kien-so">
                                                <span>Thiết Bị Số - Phụ Kiện Số</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/thiet-bi-am-thanh/c8215">
                                                <span>Thiết bị âm thanh</span>
                                            </a>
                                        </li>
                                        <li style="width: 850px;">
                                            <a href="/tai-nghe-nhac/c1804">
                                                <span>Tai nghe</span>
                                            </a>
                                        </li>-->
                </ul>
            </div>
        </div>

        <div class="wrap">
            <div class="container-full">
                <div class="row-style-1">
                    <div class="menu-left">
                        <div class="profiles">
                            <p class="image"><img src="" height="45" width="45" alt=""></p>
                            <p class="name">Tài khoản của</p>
                            <h6>Nguyễn Văn Khải</h6>
                        </div>
                        <div class="menu dropdown">
                            <button class="btn btn-default dropdown-toggle btn-block" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                Danh mục <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li class="">
                                    <a href=""><i class="ico ico-user"></i> <span>Thông tin tài khoản</span><!-- <span class='noti-nav'>Mới</span> --></a>
                                </li>
                                <li class="active">
                                    <a href="OrderServlet"> <i class="ico ico-ic-my-order"></i> <span>Quản lý đơn hàng</span></a>
                                </li>

                                <!--  -->
                                <!-- <li class="hidden-md hidden-lg">
                                    <a href="https://tiki.vn/customer/account/logout">Thoát tài khoản</a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="content-right">
                        <h1 class="have-margin">Đơn hàng của tôi</h1>  

                        <div class="dashboard-order have-margin">
                            <table class="table-responsive-2 list">
                                <thead>
                                    <tr>
                                        <th>
                                            <span class="hidden-xs hidden-sm hidden-md">Mã đơn hàng</span>
                                            <span class="hidden-lg">Code</span>
                                        </th>
                                        <th>Ngày mua</th>
                                        <th>Sản phẩm</th>
                                        <th>Tổng tiền</th>
                                        <th>
                                            <span class="hidden-xs hidden-sm hidden-md">Trạng thái <br> đơn hàng</span>
                                            <span class="hidden-lg">Trạng thái</span>
                                        </th>
                                        <!--                            <th></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${not empty ORDERLIST}">
                                        <c:forEach items="${ORDERLIST}"  var="entry">
                                            <tr>
                                                <td><a href="/OrderDetailServlet?orderDetail=${entry.key.orderId}">${entry.key.orderId}</a></td>
                                                <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${entry.key.orderDate}" /></td>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" value="${entry.value}" maxIntegerDigits="10"/> &nbsp;₫</td>
                                                
                                            </tr>
                                        </c:forEach>
                                    </c:if>

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </body>

</html>