<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en" >

    <head>
        <meta charset="UTF-8">
        <title>Login Page</title>



        <link rel="stylesheet" href="asset/style.css">

        <script>
            function toggleForm() {
                var login = document.getElementById("loginForm");
                var register = document.getElementById("registerForm");

                if (login.style.display === 'none') {
                    login.style.display = 'block';
                    register.style.display = 'none';
                } else {
                    register.style.display = 'block';
                    login.style.display = 'none';
                }
            }
        </script>
        <script src="asset/showProduct.js"></script>
    </head>

    <body>

        <div class="login-page">
            <div class="form">

                <form class="register-form" id="registerForm">
                    <p></p>
                    <input type="text" placeholder="name" name="username" />
                    <input type="password" placeholder="password" name="password"/>
                    <input type="email" placeholder="email address" name="email"/>
                    <input type="fullname" placeholder="fullname (this name will appear when you login) " name="fullname"/>
                    <button onclick="register()" type="button">create</button>
                    <p class="message" >Already registered? <a href="#" onclick="toggleForm()">Sign In</a></p>
                </form>
                <form action="LoginServlet" method="POST" id="loginForm">
                    <c:if test="${not empty requestScope.INVALID_ACCOUNT}">
                        <p class="message" style="color: red;margin-bottom: 10px">INVALID USERNAME or PASSWORD</p>
                    </c:if>
                    <input type="text" placeholder="username" name="username"/>
                    <input type="password" placeholder="password" name="password"/>
                    <button>login</button>
                    <p class="message">Not registered? <a href="#" onclick="toggleForm()">Create an account</a></p>
                </form>

            </div>
        </div>

    </body>

</html>
