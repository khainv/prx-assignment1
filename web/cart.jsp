<%-- 
    Document   : showProduct
    Created on : Mar 10, 2018, 10:19:50 PM
    Author     : khai
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html class="js no-touchevents" lang="" style="">

    <head>
        <title>CART </title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="stylesheet" href="asset/cart.css" type="text/css">
        <link rel="stylesheet" href="asset/Untitled-2.css" type="text/css"> 

        <link href="asset/fontRoboto.css" rel="stylesheet">
        <style type="text/css">
            .fancybox-margin {
                margin-right: 17px;
            }
        </style>
        <script src="asset/showProduct.js" ></script>
    </head>

    <body class="tiki-cart chrome" onload="getData()">
        <header id="header" class="nav-collapse flash-sale">
            <div class="header-form-container">
                <div class="container">
                    <a href="/Assigment1/InitStoreServlet" class="logo">
                        <img src="asset/shopping-cart.svg" width="50px"/>
                    </a>
                    <div class="form-search" >
                        <form id="search_form" action="" method="get">
                            <div class="search-wrap">
                                <div class="input">
                                    <div class="flex">
                                        <input type="text" name="q" autocomplete="off"  onkeyup="typewatch(function () {
                                                    searchSuggestion();
                                                }, 1000);"/>
                                    </div>
                                </div>
                                <button type="submit">
                                    <i class="tikicon icon-search"></i>
                                    <span>Tìm kiếm</span>
                                </button>
                            </div>
                        </form>
                        <div id="search-suggestion" style="display: none" onfocusout="outFocusSearch()">
                            <div data-reactroot="" class="search-suggestion" style="display: none;">
                            </div>
                        </div>
                        <div id="search-autocomplete" style="display: none">
                            <div data-reactroot="" class="keyword-remember" style="display: none;"></div>
                        </div>
                    </div>
                    <div class="header-link">
                        <div class="user-profile item" id="header-user">
                            <div data-reactroot="" onmouseover="showUserNav(this)" onmouseout="hideUserNav(this)">
                                <div>
                                    <i class="ico ico-user"></i>
                                    <c:if test="${not empty sessionScope.FULLNAME}">
                                        <b><c:out value="${sessionScope.FULLNAME}"/></b>
                                    </c:if>
                                    <c:if test="${empty sessionScope.FULLNAME}">
                                        <b>Đăng nhập</b>
                                    </c:if>
                                    <br>
                                    <small>Tài khoản</small>
                                </div>
                                <div class="box">
                                    <ul class="user-ajax-guest">
                                        <li id="login_link">
                                            <a href="#" class="user-name-login">
                                                <span class="text">Xem thông tin đơn hàng</span>
                                            </a>
                                        </li>
                                        <li class="user-name-register">
                                            <a href="LogoutServlet" title="Đăng xuất">
                                                <span class="text">Đăng xuất</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="header-cart" style="cursor: pointer">
                            <a data-reactroot="" rel="nofollow" href="CartServlet" class="header-cart item">
                                <i class="ico ico-cart"></i>
                                <!-- react-text: 3 -->Giỏ hàng
                                <!-- /react-text -->
                                <span class="cart-count">0</span>
                                <div class="add-to-cart-success">
                                    <span class="close">
                                        <i class="tikicon icon-circle-close"></i>
                                    </span>
                                    <p class="text">
                                        <i class="tikicon icon-circle-tick"></i>
                                        <!-- react-text: 10 -->Thêm vào giỏ hàng thành công!
                                        <!-- /react-text -->
                                    </p>
                                    <button class="btn">Xem giỏ hàng và thanh toán</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-nav">
                <div class="container">




                </div>
            </div>
            <div id="main-ajax-recentlyviewed" data-impress-list-title="Header | Sản phẩm bạn đã xem">
                <div data-reactroot="" class="product-recently-content swiper-carousel-wrapper"></div>
            </div>
        </header>
        <!-- login box -->
        <div class="modal" id="login-form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="tiki-icons tiki-icons-close"></i>
                    </button>
                    <div class="modal-body">
                        <div class="content-left">
                            <h2>Đăng nhập</h2>
                            <p>Đăng nhập để theo dõi đơn hàng, lưu
                                <br>danh sách sản phẩm yêu thích, nhận
                                <br> nhiều ưu đãi hấp dẫn.</p>
                            <img src="https://tiki.vn/desktop/img/graphic-map.png">
                        </div>
                        <div class="content-right">
                            <div class="tab">
                                <a class="tab-item active">
                                    Đăng nhập
                                </a>
                                <a class="tab-item" id="create-account" href="#" data-dismiss="modal" data-toggle="modal" data-target="#register-form">
                                    Tạo tài khoản
                                </a>
                            </div>
                            <form class="content bv-form" method="POST" action="https://tiki.vn/customer/account/ajaxLogin?ref=https%253A%252F%252Ftiki.vn%252Ftai-nghe-nhac%252Fc1804%253Fsrc%253Dmega-menu"
                                  id="login_popup_form" novalidate="novalidate">
                                <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="form-group popup_email has-feedback" id="popup_login">
                                    <label class="control-label">Email / SĐT</label>
                                    <input id="popup-login-email" type="text" class="form-control login" name="email" placeholder="Nhập Email hoặc Số điện thoại"
                                           data-bv-field="email">
                                    <i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
                                    <span class="help-block ajax-message"></span>
                                    <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập Email hoặc Số điện thoại</small>
                                    <small class="help-block" data-bv-validator="regexp"
                                           data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Email hoặc Số điện thoại không hợp lệ</small>
                                </div>
                                <div class="form-group popup_password has-feedback" id="popup_password">
                                    <label class="control-label">Mật khẩu</label>
                                    <input type="password" id="login_password" class="form-control login" name="password" placeholder="Mật khẩu từ 6 đến 32 ký tự"
                                           autocomplete="off" data-bv-field="password">
                                    <i class="form-control-feedback" data-bv-icon-for="password" style="display: none;"></i>
                                    <span class="help-block ajax-message"></span>
                                    <small class="help-block" data-bv-validator="stringLength" data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Mật khẩu phải dài từ 6 đến 32 ký tự</small>
                                </div>
                                <div class="login-ajax-captcha" style="display:none">
                                    <div id="login-popup-recaptcha"></div>
                                </div>
                                <div class="form-group" id="error_captcha" style="margin-bottom: 15px">
                                    <span class="help-block ajax-message"></span>
                                </div>
                                <div class="form-group">
                                    <p class="reset">Quên mật khẩu? Nhấn vào
                                        <a data-toggle="modal" data-target="#reset-password-form"
                                           href="#">đây</a>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="login_popup_submit" class="btn btn-info btn-block">Đăng nhập</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end login box -->
        <!-- reset password -->
        <div class="modal" id="reset-password-form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tiki-icons tiki-icons-close"></i>
                        </button>
                        <div class="head">
                            <h2>Quên mật khẩu?</h2>
                            <p>
                                <span>Vui lòng gửi email. Chúng tôi sẽ gửi link khởi tạo mật khẩu mới qua email của bạn.</span>
                            </p>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="https://tiki.vn/customer/account/ajaxForgotPassword" class="content" id="reset_popup_form">
                            <div id="forgot_successful">
                                <span></span>
                            </div>
                            <div class="form-group" id="forgot_pass">
                                <input type="text" name="email" id="email-forgot" class="form-control" value="" required="required" placeholder="Nhập email">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group last">
                                <button type="button" id="reset_form_submit" class="btn btn-info">Gửi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end reset password -->
        <!-- registration box -->
        <div class="modal" id="register-form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="tiki-icons tiki-icons-close"></i>
                    </button>
                    <div class="modal-body">
                        <div class="content-left">
                            <h2>Tạo tài khoản</h2>
                            <p>Tạo tài khoản để theo dõi đơn hàng, lưu
                                <br> danh sách sản phẩm yêu thích, nhận
                                <br>nhiều ưu đãi hấp dẫn.</p>
                            <img src="https://tiki.vn/desktop/img/graphic-map.png">
                        </div>
                        <div class="content-right">
                            <div class="tab">
                                <a class="tab-item" data-toggle="modal" data-target="#login-form">
                                    Đăng nhập
                                </a>
                                <a class="tab-item active" id="create-account" href="#" data-dismiss="modal" data-toggle="modal" data-target="#register-form">
                                    Tạo tài khoản
                                </a>
                            </div>
                            <form class="content bv-form" method="POST" action="https://tiki.vn/customer/account/ajaxCreate" id="register_popup_form"
                                  novalidate="novalidate">
                                <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="left-col">
                                    <div form-group="" id="general_error">
                                        <span></span>
                                    </div>
                                    <div class="form-group" id="register_name">
                                        <label class="control-label" for="pasword">Họ tên:</label>
                                        <div class="input-wrap has-feedback">
                                            <input type="text" class="form-control register" name="full_name" id="name" placeholder="Nhập họ tên" data-bv-field="full_name">
                                            <i class="form-control-feedback bv-no-label" data-bv-icon-for="full_name"
                                               style="display: none;"></i>
                                            <span class="help-block ajax-message"></span>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="full_name" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập họ tên</small>
                                        </div>
                                    </div>
                                    <div class="form-group" id="register_email">
                                        <label class="control-label" for="email">Email / SĐT:</label>
                                        <div class="input-wrap has-feedback">
                                            <input type="text" class="form-control register register-email-input" name="email" id="email" placeholder="Nhập Email hoặc Số điện thoại"
                                                   data-bv-field="email">
                                            <i class="form-control-feedback bv-no-label" data-bv-icon-for="email" style="display: none;"></i>
                                            <span class="help-block ajax-message"></span>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập Email Hoặc Số điện thoại</small>
                                            <small class="help-block" data-bv-validator="remote"
                                                   data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Email hoặc Số điện thoại đã tồn tại</small>
                                        </div>
                                    </div>
                                    <div class="form-group" id="register_password">
                                        <label class="control-label" for="pasword">Mật khẩu:</label>
                                        <div class="input-wrap has-feedback">
                                            <input type="password" class="form-control register" name="password" id="password" placeholder="Mật khẩu từ 6 đến 32 ký tự"
                                                   autocomplete="off" data-bv-field="password">
                                            <i class="form-control-feedback bv-no-label" data-bv-icon-for="password"
                                               style="display: none;"></i>
                                            <span class="help-block ajax-message"></span>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng nhập mật khẩu</small>
                                            <small class="help-block" data-bv-validator="stringLength"
                                                   data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Mật khẩu phải dài từ 6 đến 32 ký tự</small>
                                        </div>
                                    </div>
                                    <div class="form-group gender-select-wrap" id="register_name">
                                        <label class="control-label" for="pasword">Giới tính:</label>
                                        <div class="input-wrap has-feedback">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label for="male" class="icheck-wrap gender-select">
                                                        <div class="iradio_square-blue" style="position: relative;">
                                                            <input type="radio" name="gender" value="on" id="male" class="gender"
                                                                   data-bv-field="gender" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        Nam
                                                    </label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="female" class="icheck-wrap gender-select">
                                                        <div class="iradio_square-blue" style="position: relative;">
                                                            <input type="radio" name="gender" value="off" id="female" class="gender"
                                                                   data-bv-field="gender" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i class="form-control-feedback" data-bv-icon-for="gender" style="display: none;"></i>
                                                        Nữ
                                                    </label>
                                                    <small class="help-block" data-bv-validator="notEmpty" data-bv-for="gender" data-bv-result="NOT_VALIDATED" style="display: none;">Vui lòng chọn giới tính</small>
                                                </div>
                                            </div>
                                            <span class="help-block">Vui lòng chọn giới tính</span>
                                        </div>
                                    </div>
                                    <div class="form-group" id="register_birthday">
                                        <label class="control-label no-lh" for="birthdate">
                                            Ngày Sinh:
                                        </label>
                                        <div class="input-wrap">
                                            <div id="birthday-picker-popup"></div>
                                            <span class="help-block ajax-message" id="span-birthday"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-wrap">
                                            <label for="subcribe" class="icheck-wrap is-small">
                                                <div class="icheckbox_square-blue checked">
                                                    <input type="checkbox" name="newsletter" class="icheck" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                    <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                </div>
                                                Nhận các thông tin và chương trình khuyến mãi của Tiki qua email.
                                            </label>
                                            <div class="input-wrap">
                                                <p class="policy">Khi bạn nhấn Đăng ký, bạn đã đồng ý thực hiện mọi giao dịch mua bán theo
                                                    <a target="_blank" href="http://hotro.tiki.vn/hc/vi/articles/201971214">điều kiện sử dụng và chính sách của Tiki</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group last">
                                        <div class="input-wrap">
                                            <button type="submit" id="register_popup_submit" class="btn btn-info btn-block btn-register-submit">Tạo tài khoản</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end registration box -->
        <div class="breadcrumb-wrap">
            <div class="container">
                <ul class="breadcrumb">
                    <li>
                        <a href="/">Trang chủ</a>
                    </li>
                    <li>
                        <a href="/thiet-bi-kts-phu-kien-so">
                            <span>Thiết Bị Số - Phụ Kiện Số</span>
                        </a>
                    </li>
                    <li>
                        <a href="/thiet-bi-am-thanh/c8215">
                            <span>Thiết bị âm thanh</span>
                        </a>
                    </li>
                    <li style="width: 850px;">
                        <a href="/tai-nghe-nhac/c1804">
                            <span>Tai nghe</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wrap">
            <div class="container have-height">
                <div class="row">

                    <c:set var="TOTALPRICE" value="${0}"/>
                    <c:set var="CART" value="${sessionScope.CART}"/>
                    <c:if test="${not empty CART }">
                        <div class="col-xs-12">
                            <h5 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>( ${fn:length(CART)} sản phẩm)</span></h5>

                        </div>
                        <div class="col-xs-8 cart-col-1" >
                            <form id="shopping-cart">
                                <c:forEach items="${sessionScope.CART}" var="entry">

                                    <fmt:formatNumber value="${entry.key.price}" pattern="####" var="pat" /> 
                                    <div class="row shopping-cart-item" id="${entry.key.id}" price="${pat}">
                                        <div class="col-xs-3 img-thumnail-custom">
                                            <p class="image">
                                                <!--image here-->
                                                <img class="img-responsive" src="${entry.key.imageLink}"/>
                                            </p>
                                        </div>
                                        <div class="col-right">
                                            <div class="box-info-product">
                                                <p class="name">
                                                    <!--link product here-->
                                                    <a target="_blank" href="/Assigment1/ProductDetailServlet?productID=${entry.key.id}" >
                                                        <c:set var="TOTALPRICE" value="${TOTALPRICE + entry.key.price*entry.value}" />
                                                        <c:out value="${entry.key.name}"/>

                                                    </a>
                                                </p>
                                                <p class="action">
                                                    <a onclick="removeProductFromCart('${entry.key.id}')" class="btn btn-link btn-item-delete" data-title="${entry.key.name}" data-item-id="${entry.key.id}" data-item-qty="${entry.value}" data-item-price="${entry.key.price}" >
                                                        Xóa
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="box-price">
                                                <p class="price">
                                                    <fmt:formatNumber type="number" value="${entry.key.price}" var="fmtPrice" maxIntegerDigits="10"/>
                                                    <c:out value="${fmtPrice}"/> ₫
                                                </p>
                                            </div>
                                            <div class="quantity-block">
                                                <div class="input-group bootstrap-touchspin">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default bootstrap-touchspin-down" type="button" onclick="decreaseQty(this)">-</button>
                                                    </span>
                                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                                    <input type="tel" class="form-control quantity-r2 quantity js-quantity-product" min="0" data-js-qty="" value="${entry.value}" style="display: block;" disabled/>
                                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default bootstrap-touchspin-up" type="button" onclick="increaseQty(this)">+</button>
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- <div class="box-info-discount"></div> -->
                                        </div>
                                    </div>


                                </c:forEach>
                            </form>




                        </div>
                        <div class="col-xs-4 cart-col-2">
                            <div id="right-affix" class="affix-top">

                                <div class="each-row">

                                    <div class="box-style fee">

                                        <p class="list-info-price">
                                            <span>Tạm tính: </span>
                                            <strong>
                                                <fmt:formatNumber type="number" value="${TOTALPRICE}" maxIntegerDigits="10"/> &nbsp;₫
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="box-style fee">
                                        <div class="total2 clearfix">
                                            <span class="text-label">Thành tiền: </span>
                                            <div class="amount">
                                                <p>
                                                    <strong><fmt:formatNumber type="number" value="${TOTALPRICE}" maxIntegerDigits="10"/> &nbsp;₫</strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-large btn-block btn-danger btn-checkout" onclick="location.href = 'CheckOutServlet';sessionStorage.removeItem('cart');return false;"
                                            >Tiến hành đặt hàng</button>

                                </div>


                            </div>
                        </div>
                    </c:if>
                    <c:if test="${empty CART }">
                        <div class="alert alert-danger">Giỏ hàng không có sản phẩm. Vui lòng thực hiện lại.</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>(0 sản phẩm)</span></h5>
                                <div class="empty-cart">
                                    <img src="./img/png/cart-empty.png" alt="Không có sản phẩm nào trong giỏ hàng của bạn."/>

                                    <p class="message">Không có sản phẩm nào trong giỏ hàng của bạn.</p>
                                    <a href="/" class="btn btn-yellow">Tiếp tục mua sắm</a>
                                </div>
                            </div>
                        </div>
                    </c:if>

                </div>
            </div>

    </body>

</html>