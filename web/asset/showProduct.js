
var count = 0;
var cells = [];
var xmlDoc = null;
var xmlHttp;

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
var typewatch = function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
}();
function loadCart() {
    console.log('cart here');
}
function searchSuggestion() {
    var searchString = document.getElementById('search_form').children[0].children[0].children[0].children[0].value;
    var searchItemBox = document.getElementById('search-suggestion');
    while (searchItemBox.children[0].lastChild) {
        searchItemBox.children[0].removeChild(searchItemBox.children[0].lastChild);
    }
    console.log('hello');
    var listProduct = Array.from(xmlDoc.getElementsByTagName('product'));
    listProduct.forEach(currentValue => {
        if (currentValue.getAttribute('name').toUpperCase().includes(searchString.toUpperCase())) {
            console.log(currentValue.getAttribute('name'));
            var fragment = document.createDocumentFragment();
            var a = document.createElement('a');
            var strong = document.createElement('strong');
            strong.textContent = currentValue.getAttribute('name');
            var div1 = document.createElement('p');
            div1.setAttribute('class', 'item');
            div1.setAttribute('category-title', currentValue.parentElement.getAttribute('categoryname'));
            div1.setAttribute('product-id', currentValue.getAttribute('id'));
            div1.setAttribute('cate-id', currentValue.parentElement.getAttribute('id'));
            div1.setAttribute('product-title', currentValue.getAttribute('name'));
            a.setAttribute('href', "/Assigment1/ProductDetailServlet?productID=" + currentValue.getAttribute('id'));
            a.appendChild(strong);
            div1.appendChild(a);
            fragment.appendChild(div1);
            searchItemBox.children[0].appendChild(fragment);
        }
    });
    searchItemBox.style.display = 'block';
    searchItemBox.children[0].style.display = 'block';
}

function outFocusSearch() {
    var searchItemBox = document.getElementById('search-suggestion');
    searchItemBox.style.display = 'none';
    searchItemBox.children[0].style.display = 'none';
}

function printProduct(cateID) {
    var productBox = document.getElementsByClassName("product-box-list")[0];
    if (productBox !== undefined) {
        while (productBox.lastChild) {
            productBox.removeChild(productBox.lastChild);
        }
    }
    if (xmlDoc !== null) {
        var listCategory = xmlDoc.getElementsByTagName("subcategory");
        if (listCategory !== undefined && listCategory.length > 0) {
            Array.from(listCategory).forEach(currentValue => {

                if (currentValue.getAttribute('id') == cateID) {
                    document.getElementsByClassName('filter-list-box')[0].children[0].textContent = capitalizeFirstLetter(currentValue.getAttribute('categoryname')) + ": ";
                    var listProduct = Array.from(currentValue.getElementsByTagName('product'));
                    document.getElementsByClassName('filter-list-box')[0].children[1].textContent = listProduct.length + " kết quả";
                    listProduct.forEach(currentValue1 => {
                        var fragment = document.createDocumentFragment();
                        var div1 = document.createElement('div');
                        div1.setAttribute('class', 'product-item');
                        div1.setAttribute('category-title', currentValue.getAttribute('categoryname'));
                        div1.setAttribute('category-id', currentValue.getAttribute('id'));
                        var div2 = document.createElement('div');
                        div2.setAttribute('product-id', currentValue1.getAttribute('id'));
                        div2.setAttribute('data-title', currentValue1.getAttribute('name'));
                        div2.setAttribute('class', 'product-item');
                        var a1 = document.createElement('a');
                        a1.setAttribute('href', "/Assigment1/ProductDetailServlet?productID=" + currentValue1.getAttribute('id'));
                        var span1 = document.createElement('span');
                        span1.setAttribute('class', 'image');
                        var img1 = document.createElement('img');
                        img1.setAttribute('class', 'product-image img-responsive');
                        img1.setAttribute('src', currentValue1.getAttribute('imageLink'));
                        var span2 = document.createElement('span');
                        span2.setAttribute('class', 'title');
                        span2.textContent = currentValue1.getAttribute('name');
                        var p1 = document.createElement('p');
                        p1.setAttribute('class', 'price-sale');
                        p1.textContent = addCommas(currentValue1.getAttribute('price')) + "₫";
                        span2.appendChild(p1);
                        span1.appendChild(img1);
                        a1.appendChild(span1);
                        a1.appendChild(span2);
                        div2.appendChild(a1);
                        div1.appendChild(div2);
                        fragment.appendChild(div1);
                        productBox.appendChild(fragment);
                    });
                }
            });
        }
    }
}

function addCategoryToLeftNav(xmlDoc) {
    var leftNav = document.getElementsByClassName("main-nav-wrap")[0].children[1];
    var fragment = document.createDocumentFragment();
    var listCategory = xmlDoc.getElementsByTagName("category");
    [].map.call(
            listCategory,
            function (currentValue, index, collection) {
                if (currentValue.getAttribute('parentID') === null) {
                    var li = document.createElement('li');
                    var a = document.createElement('a');
                    var span = document.createElement('span');
                    span.textContent = currentValue.getAttribute('categoryName');
                    a.setAttribute('href', currentValue.getAttribute('categoryLink'));
                    li.appendChild(a);
                    fragment.appendChild(li);
                    leftNav.appendChild(fragment);
                }
            }
    );
}

function showSubCategory(cateDiv) {
    cateDiv.children[0].setAttribute('class', 'active');
    cateDiv.children[1].style.display = 'block';
}

function hideSubCategory(cateDiv) {
    cateDiv.children[0].removeAttribute('class');
    cateDiv.children[1].style.display = 'none';
}
function crawler(){
    window.location = '/Assigment1/InitStoreServlet';
}
function getData() {
    var url = "http://localhost:8080/Assigment1/ResponseStoreServlet";
    fetch(url)
            .then(response => response.text())
            .then(str => (new window.DOMParser()).parseFromString(str, "text/xml"))
            .then(data => {
                xmlDoc = data;
                console.log(data);
                showCartError();
                countProductInCart();
            });

}
function capitalizeFirstLetter(string) {
    console.log(string.toLowerCase());
    string = string.toLowerCase();
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function printAllProduct(cateID) {
    var productBox = document.getElementsByClassName("product-box-list")[0];
    if (productBox !== undefined) {
        while (productBox.lastChild) {
            productBox.removeChild(productBox.lastChild);
        }
    }
    if (xmlDoc !== null) {

        var listCategory = Array.from(xmlDoc.getElementsByTagName("category"));
        if (listCategory !== undefined && listCategory.length > 0) {

            listCategory.forEach(currentValue => {

                if (currentValue.getAttribute('id') == cateID) {
                    document.getElementsByClassName('filter-list-box')[0].children[0].textContent = capitalizeFirstLetter(currentValue.getAttribute('categoryname')) + ": ";
                    var listProduct = Array.from(currentValue.getElementsByTagName('product'));
                    document.getElementsByClassName('filter-list-box')[0].children[1].textContent = listProduct.length + " kết quả";
                    listProduct.forEach(currentValue1 => {
                        var fragment = document.createDocumentFragment();
                        var div1 = document.createElement('div');
                        div1.setAttribute('class', 'product-item');
                        div1.setAttribute('category-title', currentValue.getAttribute('categoryname'));
                        div1.setAttribute('category-id', currentValue.getAttribute('id'));
                        var div2 = document.createElement('div');
                        div2.setAttribute('product-id', currentValue1.getAttribute('id'));
                        div2.setAttribute('data-title', currentValue1.getAttribute('name'));
                        div2.setAttribute('class', 'product-item');
                        var a1 = document.createElement('a');
                        a1.setAttribute('href', "/Assigment1/ProductDetailServlet?productID=" + currentValue1.getAttribute('id'));
                        var span1 = document.createElement('span');
                        span1.setAttribute('class', 'image');
                        var img1 = document.createElement('img');
                        img1.setAttribute('class', 'product-image img-responsive');
                        img1.setAttribute('src', currentValue1.getAttribute('imageLink'));
                        var span2 = document.createElement('span');
                        span2.setAttribute('class', 'title');
                        span2.textContent = currentValue1.getAttribute('name');
                        var p1 = document.createElement('p');
                        p1.setAttribute('class', 'price-sale');
                        p1.textContent = addCommas(currentValue1.getAttribute('price')) + "₫";
                        span2.appendChild(p1);
                        span1.appendChild(img1);
                        a1.appendChild(span1);
                        a1.appendChild(span2);
                        div2.appendChild(a1);
                        div1.appendChild(div2);
                        fragment.appendChild(div1);
                        productBox.appendChild(fragment);
                    });
                }
            });
        }
    }
}
function addToCart(id) {
    if (typeof (sessionStorage) !== "undefined") {
        if (sessionStorage.cart == null) {
            sessionStorage.cart = '<xml></xml>';
        }
        var p = new DOMParser();
        var qty = document.getElementById('qty').value;
        if (qty > 0) {
            var cartDoc = stringToXML(sessionStorage.cart);
            var listProduct = Array.from(cartDoc.getElementsByTagName("product"));
            if (listProduct !== null && listProduct.length !== 0) {
                var found = listProduct.find(product => product.getAttribute('id') == id);
                if (found != null) {
                    found.setAttribute('qty', parseInt(found.getAttribute('qty')) + parseInt(qty));
                } else {
                    var newproduct = document.createElement('product');
                    newproduct.setAttribute('id', id);
                    newproduct.setAttribute('qty', qty);
                    cartDoc.documentElement.append(newproduct);
                }
            } else {
                var newproduct = document.createElement('product');
                newproduct.setAttribute('id', id);
                newproduct.setAttribute('qty', qty);
                cartDoc.documentElement.append(newproduct);
            }
        }
        serializeToString(cartDoc);
        countProductInCart();
        sendCart();
        console.log(sessionStorage.cart);
    } else {
        console.log("browser is not supported storage!!");
    }
}
function setProductQty(id, qty) {
    if (typeof (sessionStorage) !== "undefined") {
        if (sessionStorage.cart == null) {
            sessionStorage.cart = '<xml></xml>';
        }
        if (qty > 0) {
            var cartDoc = stringToXML(sessionStorage.cart);
            var listProduct = Array.from(cartDoc.getElementsByTagName("product"));
            if (listProduct != null && listProduct.length !== 0) {
                var found = listProduct.find(product => product.getAttribute('id') == id);
                if (found != null) {
                    found.setAttribute('qty', parseInt(qty));
                }
            }
        }
        serializeToString(cartDoc);
        countProductInCart();
        console.log(sessionStorage.cart);
    } else {
        console.log("browser is not supported storage!!");
    }
}
function removeProductFromCart(id) {
    if (typeof (sessionStorage) !== "undefined") {
        if (sessionStorage.cart == null) {
            sessionStorage.cart = '<xml></xml>';
        }
        var cartDoc = stringToXML(sessionStorage.cart);
        var listProduct = Array.from(cartDoc.getElementsByTagName("product"));
        if (listProduct != null) {
            listProduct.forEach(product => {
                if (product.getAttribute('id') === id) {
                    product.remove();
                }
            });
        }
        document.getElementById(id).remove();
        serializeToString(cartDoc);
        countProductInCart();
        sendCartWithoutRedirect();
        console.log(sessionStorage.cart);
        countTotalPrice();
    } else {
        console.log("browser is not supported storage!!");
    }
}
function sendCart() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (xhttp.responseText == 'COMPLETE_CREATE_XML_CART') {
                var elm = document.getElementsByClassName("add-to-cart-success")[0];
                elm.style.display = 'block';
            }
        }
    };
    xhttp.open("POST", "http://localhost:8080/Assigment1/api/cart/addToCart", true);
    xhttp.send(sessionStorage.cart);
}
function redirectToCart(){
    window.location = 'cart.jsp';
}
function sendCartWithoutRedirect() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (xhttp.responseText == 'COMPLETE_CREATE_XML_CART') {
                console.log('send cart to server');
            }
        }
    };
    xhttp.open("POST", "http://localhost:8080/Assigment1/api/cart/addToCart", true);
    xhttp.send(sessionStorage.cart);
}
function serializeToString(originalDom) {
    var xml = new XMLSerializer().serializeToString(originalDom);
    sessionStorage.setItem("cart", xml);
}
function stringToXML() {
    var xml = sessionStorage.getItem("cart");
    var restoredDom = new DOMParser().parseFromString(xml, "text/xml");
    return restoredDom;
}
function countProductInCart() {
    if (typeof (sessionStorage) !== "undefined") {
        if (sessionStorage.cart == null) {
            sessionStorage.cart = '<xml></xml>';
        }
        var count = 0;

        var cartDoc = stringToXML(sessionStorage.cart);
        var listProduct = Array.from(cartDoc.getElementsByTagName("product"));
        if (listProduct != null && listProduct.length !== 0) {
            count = listProduct.length;
        } else {
            count = 0;
        }
        document.getElementsByClassName('cart-count')[0].textContent = count;
        serializeToString(cartDoc);
        console.log(sessionStorage.cart);
    } else {
        console.log("browser is not supported storage!!");
    }
}
function decreaseQty(currentDiv) {
    var currentQty = parseInt(currentDiv.parentElement.parentElement.children[2].value);

    if (currentQty > 1) {
        currentDiv.parentElement.parentElement.children[2].value = currentQty - 1;
        var id = currentDiv.parentElement.parentElement.parentElement.parentElement.children[0].children[1].children[0].getAttribute('data-item-id');
        setProductQty(id, currentQty - 1);
        sendCartWithoutRedirect();
        setProductQty(id, currentQty - 1);
        sendCartWithoutRedirect();
        //var price = parseInt(document.getElementById(id).getAttribute('price'));
        //document.getElementById(id).getElementsByClassName('price')[0].textContent = Number(price * (parseInt(currentQty) - 1)).toLocaleString() + " ₫";
        countTotalPrice();
    } else {
        alert('Cannot decrease quantity. Please remove the product out of the cart');
    }
}
function increaseQty(currentDiv) {
    var currentQty = parseInt(currentDiv.parentElement.parentElement.children[2].value);

    currentDiv.parentElement.parentElement.children[2].value = currentQty + 1;
    var id = currentDiv.parentElement.parentElement.parentElement.parentElement.children[0].children[1].children[0].getAttribute('data-item-id');
    setProductQty(id, currentQty + 1);
    sendCartWithoutRedirect();
    //var price = parseInt(document.getElementById(id).getAttribute('price'));
    //document.getElementById(id).getElementsByClassName('price')[0].textContent = Number(price * (parseInt(currentQty) + 1)).toLocaleString() + " ₫";
    countTotalPrice();
}
function showCartError() {
    if (typeof (sessionStorage) !== undefined) {
        if (sessionStorage.cart != null) {
            if (sessionStorage.cart.includes('error')) {
                alert('Cart Error. Please try to add again.');
                sessionStorage.removeItem('cart');
                window.location = '/Assigment1';
            }
        }
    }
}
function countTotalPrice() {
    var listProduct = document.getElementsByClassName('row shopping-cart-item');
    var totalPrice = 0;
    if (listProduct !== null && listProduct.length > 0) {
        Array.from(listProduct).forEach(p => {
            var qty = p.getElementsByClassName('form-control quantity-r2 quantity js-quantity-product')[0].value;
            totalPrice = totalPrice + parseInt(p.getAttribute('price')) * qty;
            });
        document.getElementsByClassName('list-info-price')[0].children[1].textContent = Number(totalPrice).toLocaleString() + " ₫";
        document.getElementsByClassName('total2 clearfix')[0].children[1].children[0].children[0].textContent = Number(totalPrice).toLocaleString() + " ₫";
    }
}
function showUserNav(div) {
    div.getElementsByClassName('box')[0].style.display = 'block';
}
function hideUserNav(div) {
    div.getElementsByClassName('box')[0].style.display = 'none';
}
function showLoginModal() {
    document.getElementById('login-form').style.display = 'block';
    document.getElementById('login-form').style.paddingRight = '17px';
    document.getElementById('login-form').classList.add('in');
    document.getElementById('header-user').getElementsByClassName('box')[0].style.display = 'none';

}

function hideOnClickOutside(element) {
    console.log('hello');
    document.getElementById('login-form').style.paddingRight = '0px';
    document.getElementById('login-form').style.display = 'none';
    document.getElementById('header-user').getElementsByClassName('box')[0].style.display = 'none';

}

function register(){
        var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
                var p = document.getElementById('registerForm').children[0];
                p.textContent = "Register Succesfully";
                p.style.color = 'green';
                
        }else{
            var p =document.getElementById('registerForm').children[0];
            p.style.color = 'red';
            p.textContent = 'Register Failed';
        }
    };
    var register = document.getElementById("registerForm");
    var param = 'username='+register.children[1].value + 
            '&password='+register.children[2].value + 
            '&email='+register.children[3].value + 
            '&fullname='+register.children[4].value;
    xhttp.open("POST", "http://localhost:8080/Assigment1/api/user/register", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(param);
}


