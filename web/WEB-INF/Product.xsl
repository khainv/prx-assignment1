<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Product.xsl
    Created on : March 13, 2018, 3:29 PM
    Author     : khai
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"  indent="yes" encoding="UTF-8"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="store">
        <xsl:for-each select="category">
            <xsl:for-each select="product">
                <div class="row product-summary">
                    <div class="product-image">
                        <div class="image-box js-image-box">
                            <div class="magiczoom  ">
                                <a class="zoom-wrap-link">
                                    <div style="height:387px;width:387px;" class="zoomWrapper">
                                        <img id="product-magiczoom" 
                                             style="position: absolute;">
                                            <xsl:attribute name="src">
                                                <xsl:value-of select="@imageLink" /> 
                                            </xsl:attribute>
                                        </img>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="product-cart">
                        <div class="item-box">
                            <h1 class="item-name" id="product-name">
                                <xsl:value-of select="@name" />           
                            </h1>
                            <input id="product_id" name="id" type="hidden" value="756522"/>
                            <input id="product_price" name="price" type="hidden" value="8190000"/>
                            <input id="productset_name" name="category" type="hidden" value="Điện thoại - Máy tính bảng"/>
                            <div class="row flex">
                                <div class="col-xs-7 no-padding-right product-info-block">
                                    <div class="item-row1">
                                        <div class="item-price">
                                            <div class="price-block show-border">
                                                <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                                    <meta itemprop="priceCurrency" content="VND"/>
                                                    <meta itemprop="price" content="8190000"/>
                                                    <link itemprop="availability" href="http://schema.org/InStock"/>
                                                </div>
                                                <p class="special-price-item" data-value="8190000" id="p-specialprice">
                                                    <span id="flash-sale-price-label" style="display: none;" class="">
                                                        <img class="icon-flash-sale" src="https://vcdn.tikicdn.com/desktop/img/flash-sale-price-label.png?v=2" width="80"/>
                                                        <img class="icon-hot-deal" src="https://vcdn.tikicdn.com/desktop/img/deal-hot@2x.png" width="91"/>Giá: </span>
                                                    <span id="span-price">
                                                        <xsl:value-of select='format-number(@price, "###,###,###")' /> ₫
                                                    </span>
                                                    <span class="vat">Đã có VAT</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <form role="form" id="add-to-cart" action="/checkout/ajaxAdd">
                                        <input type="hidden" name="product_type" value="configurable"/>
                                                            
                                        <div class="item-product-options">
                                            <!-- BEGIN ADD TO CART -->
                                            <div id="add-cart-action">
                                                <div class="add-cart-action" style="display: block">
                                                    <div class="quantity-box">
                                                        <input id="product_id_for_wishlist" name="id" type="hidden" value="756522"/>
                                                    
                                                        <div class="quantity-col1">
                                                            <p class="quantity-label">Số lượng:</p>
                                                            <p class="tiki-number-input">
                                                                <div class="input-group bootstrap-touchspin">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default bootstrap-touchspin-down" type="button" >
                                                                            <xsl:attribute name="onClick">
                                                                                <xsl:text>decreaseQty(this)</xsl:text>
                                                                            </xsl:attribute>
                                                                            -
                                                                        </button>
                                                                    </span>
                                                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                                                    <input id="qty" type="tel" name="qty" value="1" min="1" max="100" class="form-control" style="display: block;"/>
                                                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default bootstrap-touchspin-up" type="button">
                                                                            <xsl:attribute name="onClick">
                                                                                <xsl:text>increaseQty(this)</xsl:text>
                                                                            </xsl:attribute>
                                                                            +
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </p>
                                                        </div>                                                    
                                                        <div class="cta-box">
                                                        
                                                        
                                                            <button class="add-to-cart  js-add-to-cart is-css" type="button">
                                                                <xsl:attribute name="onClick">
                                                                    <xsl:text>addToCart(</xsl:text>
                                                                    <xsl:value-of select="@id" /> 
                                                                    <xsl:text>)</xsl:text>
                                                                </xsl:attribute>
                                                                <span class="text">
                                                                    Thêm Vào Giỏ Hàng                                                            </span>
                                                            </button>

                                                            <a href="javascript:" class="add-to-wishlist  is-css">
                                                                <span class="icon js-product-gift-icon" data-placement="bottom" data-toggle="tooltip" data-title="Thêm Vào Yêu Thích">
                                                                    <i class="ico ico-ic-fav"></i>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
