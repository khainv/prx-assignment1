<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : NavLeft.xsl
    Created on : March 12, 2018, 1:07 AM
    Author     : khai
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" indent="yes" />

    <xsl:template match="/">
       
        <xsl:apply-templates/>
        
            
    </xsl:template>
    <xsl:template match="store">
        <xsl:for-each select="category">
            <li>
                
                <xsl:attribute name="onmouseover">
                    <xsl:text>showSubCategory(this)</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="onmouseout">
                    <xsl:text>hideSubCategory(this)</xsl:text>
                </xsl:attribute>
                <a>
                    <xsl:attribute name="data-id">
                        <xsl:value-of select="@id" />
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:text>printAllProduct(</xsl:text>
                        <xsl:value-of select="@id" /> 
                        <xsl:text>)</xsl:text>
                    </xsl:attribute>
                    <span>
                        <xsl:value-of select="@categoryname" />
                    </span>
                    
                </a>
                <div class="nav-sub" style="display: none;">
                    <xsl:for-each select="subcategory">
                    
                        <ul>
                            <li>
                                
                                <div class="nav-sub-list-box">
                                    <a>
                                        <xsl:attribute name="onClick">
                                            <xsl:text>printProduct(</xsl:text>
                                            <xsl:value-of select="@id" /> 
                                            <xsl:text>)</xsl:text>
                                        </xsl:attribute>
                                        <xsl:value-of select="@categoryname" />
                                    </a>
                                </div>
                            </li>
                        </ul>
                    
                    </xsl:for-each>
                </div>
            </li>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
