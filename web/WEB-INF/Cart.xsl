<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Cart.xsl
    Created on : March 13, 2018, 9:28 PM
    Author     : khai
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="xml">
        <xsl:for-each select="product">
            
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
