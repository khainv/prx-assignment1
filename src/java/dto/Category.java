/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author khai
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Category", propOrder = {"link", "productList", "subCategory"})
@XmlRootElement(name = "category")
@Entity
@Table(name = "Category")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CategoryID")
    @XmlAttribute(name = "id", required = true)
    private String id;
    @Column(name = "CategoryName")
    @XmlAttribute(name = "categoryname")
    private String name;
    @Column(name = "CategoryLink")
    @XmlAttribute(name = "categorylink")
    private String link;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "AncestorID", nullable = true)
    @XmlTransient
    private Category ancestorId;
    @Transient
    @XmlTransient
    private String realIDofParent;
    @Transient
    @XmlAttribute(name = "parentid", required = false)
    private String parentID;
    @Transient
    @XmlAttribute(name = "parentname", required = false)
    private String parentName;
    
    @XmlElement(name = "subcategory")
    @Transient
    private List<Category> subCategory;

    public List<Category> getSubCategory() {
        if(subCategory == null){
            subCategory = new ArrayList<>();
        }
        return subCategory;
    }

    @XmlElement(name = "product", required = true)
    @Transient
    private List<Product> productList;

    public List<Product> getProductList() {
        if (productList == null) {
            productList = new ArrayList<>();
        }
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getRealIDofParent() {
        return realIDofParent;
    }

    public void setRealIDofParent(String realID) {
        this.realIDofParent = realID;
    }

    public Category() {
    }

    public Category getAncestorId() {
        return ancestorId;
    }

    public void setAncestorId(Category ancestorId) {
        this.ancestorId = ancestorId;
    }

    public Category(String id, String name, String link, String webPage) {
        this.id = id;
        this.name = name;
        if (link.contains("http")) {
            this.link = link;
        } else {
            this.link = webPage + link;
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
