/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author khai
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product", propOrder = {"id", "link", "imageLink", "price", "name", "description"})
@XmlRootElement(name = "product")
@Entity
@Table(name = "Product")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ProductID")
    @XmlAttribute(required = true)
    private int id;

    @Column(name = "ProductLink")
    @XmlAttribute(required = true)
    private String link;
    @XmlAttribute
    @Column(name = "ImageLink")

    private String imageLink;
    @XmlAttribute(required = true)
    @Column(name = "Price")
    private BigDecimal price;
    @XmlAttribute(required = true)
    @Column(name = "ProductTitle")
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @XmlTransient
    @JoinColumn(name = "CategoryID", nullable = false)
    private Category categoryId;
    @XmlAttribute(required = false)
    @Column(name = "Description")
    private String description;


    public Product() {
    }

    public Product(int id, String link, String imageLink, BigDecimal price, String name, Category categoryId, String description, String webPage) {
        this.id = id;
        if (link.contains("http")) {
            this.link = link;
        } else {
            this.link = webPage + link;
        }
        if (imageLink.contains("http")) {
            this.imageLink = imageLink;
        } else {
            this.imageLink = webPage + imageLink;
        }

        this.price = price;
        this.name = name;
        this.categoryId = categoryId;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public String getImageLink() {
        return imageLink;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public Category getCategoryId() {
        return categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategoryId(Category categoryId) {
        this.categoryId = categoryId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", link=" + link + ", imageLink=" + imageLink + ", price=" + price + ", name=" + name + ", categoryId=" + categoryId + ", description=" + description + '}';
    }

}
