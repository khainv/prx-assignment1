/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connectDB.HibernateUtil;
import dto.Order;
import dto.OrderDetail;
import dto.Product;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author khai
 */
public class OrderDetailDAO {
    
    public List<OrderDetail> getAllOrderDetail() {
        List<OrderDetail> orderDeList = new ArrayList<OrderDetail>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            orderDeList = session.createQuery("from OrderDetail").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return orderDeList;
    }

//    public List<OrderDetail> getAllOrderDetail(Order order) {
//        List<OrderDetail> orders = new ArrayList<>();
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try {
//            Query query = session.createSQLQuery("Select * from [OrderDetail] o where o.orderID=:order");
//            query.setParameter("order", Integer.parseInt(order.getOrderId()));
//
//            orders = (List<OrderDetail>) query.list();
//           
//        } catch (RuntimeException e) {
//            e.printStackTrace();
//        } finally {
//            session.flush();
//            session.close();
//        }
//        return orders;
//    }

    public boolean addOrderDetail(OrderDetail orderdetail) {
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(orderdetail);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return false;
    }
}
