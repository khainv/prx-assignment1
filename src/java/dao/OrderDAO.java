/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connectDB.HibernateUtil;
import dto.Customer;
import dto.Order;
import dto.OrderDetail;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author khai
 */
public class OrderDAO {
//    public List<Order> getAllOrderOfOneCustomer(int customerId) {
//        
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try {
//            Query query = session.createSQLQuery("Select * from [Order] o where o.customerID=:customerId");
//            query.setInteger("customerId", customerId);
//            System.out.println(query.getQueryString());
//            return (List<Order>)query.list();
//        } catch (RuntimeException e) {
//            e.printStackTrace();
//        } finally {
//            session.flush();
//            session.close();
//        }
//        return null;
//    }
    
     public List<Order> getAllOrder() {
        List<Order> orderDeList = new ArrayList<Order>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            orderDeList = session.createQuery("from Order").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return orderDeList;
    }

    public boolean addOrder(Order order) {
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            System.out.println(order.getCustomers().getCustomerId());
            session.save(order);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return false;
    }
}
