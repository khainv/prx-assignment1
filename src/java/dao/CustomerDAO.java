/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connectDB.HibernateUtil;
import dto.Category;
import dto.Customer;
import dto.Product;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author khai
 */
public class CustomerDAO implements Serializable {

    public List<Customer> getAllCustomer() {
        List<Customer> customers = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            customers = session.createQuery("from Customer").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return customers;
    }

    public boolean addCustomer(Customer customer) {
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(customer);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return false;
    }
}
