/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import dao.CategoryDAO;
import dto.Category;
import dto.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author khai
 */
public class Crawler implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Crawler.class.getName());
    private volatile boolean running = true;

    public void terminate() {
        running = false;
    }

    private static final String DIEN_MAY_XANH_PAGE = "https://dienmayxanh.com/";
    private static final String VUI_VUI_PAGE = "https://vuivui.com";

    public void getVuiVui() {
        VuiVuiQuery query = new VuiVuiQuery();
        List<Category> listCategory = query.getCategory(VUI_VUI_PAGE);
        List<Product> listProduct = query.getAllProduct(listCategory);
        query.insertCategoryToDB(listCategory);
        query.insertProductToDB(listProduct);
        
        //remove redunant category
        CategoryDAO dao = new CategoryDAO();
        List<Category> addedDB = dao.getAllCategorys();
        List<Category> listParentCategory = addedDB.stream().filter(cate -> cate.getAncestorId()== null).collect(Collectors.toList());
        List<Category> subCategory = addedDB.stream().filter(cate -> cate.getAncestorId()!= null).collect(Collectors.toList());
        List<Category> finalCate = new ArrayList<>();
        listParentCategory.forEach(parent -> {
            List<Category> test = subCategory.stream().filter(sub -> sub.getAncestorId().getId().equalsIgnoreCase(parent.getId())).collect(Collectors.toList());
            if(test!=null){
                if(test.isEmpty()){
                    finalCate.add(parent);
                }
            }
        });
        finalCate.forEach(cate -> {
            dao.deleteCategory(cate.getId());
        });

    }

    public void getDMX() {
        String htmlString = XMLUtil.parseCategory(DIEN_MAY_XANH_PAGE);
        List<Category> listCateDMX = XMLUtil.getCategory(htmlString);
        XMLUtil.addCategoryToDB(listCateDMX);
        listCateDMX.forEach(cate
                -> {
                    System.out.println(cate.getLink());
                    List<dto.Product> listProductDMX = XMLUtil.parseProduct(cate);
                    if (listProductDMX != null) {
                        XMLUtil.addProductToDB(listProductDMX);
                    }
                });

    }
    


    @Override
    public void run() {
        while (running) {
            LOGGER.log(null, "Sleeping...");
            getDMX();
            getVuiVui();
            LOGGER.log(null, "Processing");
        }
    }

}
