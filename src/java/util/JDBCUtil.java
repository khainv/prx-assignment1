/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author HieuTNSE61591
 */
public class JDBCUtil implements Serializable {

    private static final String dbName = "ProjectXml";
    private static final String user = "sa";
    private static final String pass = "123789";

    public JDBCUtil() {
    }

    public static String createConnectionString() {
        String conString = "jdbc:sqlserver://localhost:1433;databaseName="
                + dbName + ";user=" + user + ";password=" + pass;
        return conString;
    }

    public static int CUDQuery(String conString, String sqlQuery) {
        Connection conn = null;
        Statement stmt = null;
        int rows = 0;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(conString);
            stmt = conn.createStatement();
            rows = stmt.executeUpdate(sqlQuery);
            return rows;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return 0;
    }

    public static boolean insert(String conString, String query, Object[] params) {
        Connection conn = null;
        PreparedStatement pstm = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(conString);
            pstm = conn.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                pstm.setObject(i + 1, params[i]);
            }
            System.out.println(pstm.toString());
            int result = pstm.executeUpdate();
            if (result > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    
    public static boolean getObjectByParams(String conString, String sql, Object[] param) throws SQLException {
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(conString);

            pstm = con.prepareStatement(sql);
            
            for (int i = 0; i < param.length; i++) {
                pstm.setObject(i + 1, param[i]);
            }
            rs = pstm.executeQuery();
            if(rs.next()){
                return true;
            }
            return false;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }

                if (pstm != null) {
                    pstm.close();
                }

                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
