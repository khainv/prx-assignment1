/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import connectDB.HibernateUtil;
import dto.Category;
import dto.Store;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 *
 */
public class XMLUtil implements Serializable {

    public static <T> OutputStream marshallUtil(String xmlPath, T entity, String xsdPath) {
        try {
            JAXBContext context = JAXBContext.newInstance(entity.getClass());
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            OutputStream out = new ByteArrayOutputStream();

            //validate by schema (xsd file)
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(xsdPath));
            m.setSchema(schema);

            File file = new File(xmlPath);
            m.marshal(entity, out);
            m.marshal(entity, file);
            System.out.println("Marshal " + xmlPath + " Successful!!!");
            return out;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T unmarshallUtil(String xmlPath, String xsdPath, String packagePath, OutputStream out) {
        try {
            JAXBContext context = JAXBContext.newInstance(packagePath);
            Unmarshaller unm = context.createUnmarshaller();

            //validate by schema (xsd file)
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(xsdPath));
            unm.setSchema(schema);
            File file = new File(xmlPath);
            ByteArrayOutputStream buffer = (ByteArrayOutputStream) out;
            byte[] bytes = buffer.toByteArray();
            InputStream inputStream = new ByteArrayInputStream(bytes);
            T result = (T) unm.unmarshal(inputStream);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//    public static String unicodeEscaped(char ch) {
//        if (ch < 0x10) {
//            return "\\u000" + Integer.toHexString(ch);
//        } else if (ch < 0x100) {
//            return "\\u00" + Integer.toHexString(ch);
//        } else if (ch < 0x1000) {
//            return "\\u0" + Integer.toHexString(ch);
//        }
//        return "\\u" + Integer.toHexString(ch);
//    }
//
//    public static String unicodeEscaped(String str) {
//        String returnValue = "";
//        for (int i = 0; i < str.length(); i++) {
//            returnValue += unicodeEscaped(str.charAt(i));
//        }
//        return returnValue;
//    }
    public static String changeUnicode(String text) {
        text = text.replaceAll("&#193;", "Á");
        text = text.replaceAll("&#194;", "Â");
        text = text.replaceAll("&#195;", "Ã");
        text = text.replaceAll("&#201;", "É");
        text = text.replaceAll("&#202;", "Ê");
        text = text.replaceAll("&#205;", "Í");
        text = text.replaceAll("&#212;", "Ô");
        text = text.replaceAll("&#213;", "Õ");
        text = text.replaceAll("&#211;", "Ó");
        text = text.replaceAll("&#218;", "Ú");
        text = text.replaceAll("&#225;", "á");
        text = text.replaceAll("&#226;", "â");
        text = text.replaceAll("&#227;", "ã");
        text = text.replaceAll("&#233;", "é");
        text = text.replaceAll("&#234;", "ê");
        text = text.replaceAll("&#236;", "ì");
        text = text.replaceAll("&#237;", "í");
        text = text.replaceAll("&#242;", "ò");
        text = text.replaceAll("&#244;", "ô");
        text = text.replaceAll("&#245;", "õ");
        text = text.replaceAll("&#243;", "ó");
        text = text.replaceAll("&#250;", "ú");
        text = text.replaceAll("&#192;", "À");
        text = text.replaceAll("&#224;", "à");
        text = text.replaceAll("&#249;", "ù");
        return text;
    }

    public static String changeHTMLSpecialText(String text) {
        text = text.replaceAll("&Ccedil;", "Ç");
        text = text.replaceAll("&ccedil;", "ç");
        text = text.replaceAll("&Aacute;", "Á");
        text = text.replaceAll("&Acirc;", "Â");
        text = text.replaceAll("&Atilde;", "Ã");
        text = text.replaceAll("&Eacute;", "É");
        text = text.replaceAll("&Ecirc;", "Ê");
        text = text.replaceAll("&Iacute;", "Í");
        text = text.replaceAll("&Ocirc;", "Ô");
        text = text.replaceAll("&Otilde;", "Õ");
        text = text.replaceAll("&Oacute;", "Ó");
        text = text.replaceAll("&Uacute;", "Ú");
        text = text.replaceAll("&aacute;", "á");
        text = text.replaceAll("&acirc;", "â");
        text = text.replaceAll("&atilde;", "ã");
        text = text.replaceAll("&eacute;", "é");
        text = text.replaceAll("&ecirc;", "ê");
        text = text.replaceAll("&iacute;", "í");
        text = text.replaceAll("&ocirc;", "ô");
        text = text.replaceAll("&otilde;", "õ");
        text = text.replaceAll("&oacute;", "ó");
        text = text.replaceAll("&uacute;", "ú");
        text = text.replaceAll("&Agrave;", "À");
        text = text.replaceAll("&agrave;", "à");
        text = text.replaceAll("&ugrave;", "ù");
        return text;
    }

    public static String ReadBigStringIn(BufferedReader buffIn) throws IOException {
        StringBuilder everything = new StringBuilder();
        String line;
        while ((line = buffIn.readLine()) != null) {
//            if (line.contains("img")) {
//  
//                line = line.replaceAll("<img(.*?)>", "<img$1/>");
//                line = line.replaceAll("<img(.*[^/a-zA-Z]{1})>[^<]", "<img$1/>");
//                if (line.contains("}}\"")) {
//                    line = line.replaceAll("}}\"", "");
//                }
//            }
//            if (line.contains("input")) {
//                line = line.replaceAll("<input(.*?)>", "<input$1/>");
//            }
//            if (line.contains("id=vesitem")) {
//                line = line.replaceAll("id=(vesitem-[0-9]*)", "id=\"$1\"");
//            }
//
            line = line.replaceAll("\\s&\\s", "&amp;");
//                        if (line.contains("&")) {
//                            line = line.replaceAll("&", "&amp;");
//                        }

            everything.append(line + "\n");
        }
        return everything.toString();
    }

    public static void testRegex(String regex) {
        String userInputPattern = regex;
        try {
            Pattern.compile(userInputPattern);
        } catch (PatternSyntaxException exception) {
            System.err.println(exception.getDescription());
        }
        System.out.println("Syntax is ok.");
    }

    public static String parseHTML(String filePath, String uri) {
        Writer writer = null;
        InputStream is = null;
        try {
            URL url = new URL(uri);
            URLConnection yc = url.openConnection();

            yc.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Window NT 5.0)");
            is = yc.getInputStream();

            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(filePath), "UTF-8"));
            String processString = ReadBigStringIn(in);
            processString = processString.replace("<!doctype html>", "");
            processString = processString.replaceAll("alt=\"\"\n\\s*>", "alt=\"\"/>");
            processString = processString.replace("<ul class=\"user-menu\">", "");
            processString = processString.replaceAll("<a id=\"contentarea\" name=\"contentarea\" tabindex=\"-1\"/></a>", "<a id=\"contentarea\" name=\"contentarea\" tabindex=\"-1\"></a>");
            processString = processString.replaceAll("<img.*\n{1}.*></div></div>", "</div></div>");
            processString = processString.replace("<div class=\"col col-last\" style=\"background-color: #006EC1;\">", "");
            processString = processString.replace("<main class=\"main-wrapper\"><div id=\"maincontent\" class=\"l-container1 page-main\">", "");
            processString = processString.replaceAll("<script.*>((?!<script).*\n)*<\\/script>", "");
            processString = processString.replaceAll("<script.*>.*<\\/script>", "");

            writer.write(processString);
            writer.close();
            is.close();
            return processString;

        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
        return null;
    }

    public static String parseCategory(String uri) {
        InputStream is = null;
        try {
            URL url = new URL(uri);
            URLConnection yc = url.openConnection();

            yc.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Window NT 5.0)");
            is = yc.getInputStream();

            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//            writer = new BufferedWriter(new OutputStreamWriter(
//                    new FileOutputStream(filePath), "UTF-8"));
            String processString = ReadBigStringIn(in);
            Pattern pattern = Pattern.compile("<nav>.*<\\/nav>");
            Matcher matcher = pattern.matcher(processString);
            if (matcher.find()) {
                System.out.println("found");
            } else {
                System.out.println("not found");
            }
            processString = matcher.group()
                    .replaceAll("=([^\"]*?)>", "=\"$1\">")
                    .replaceAll("=([^\"]*?)\\s", "=\"$1\"" + " ")
                    .replaceAll("<li.*?>", "");
            //writer.write(processString);
            //writer.write(changeUnicode(matcher.group().replaceAll("<div class=\"slideshow\">", "")));
//            writer.close();
            is.close();
            return processString;

        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
        return null;
    }

    public static List<dto.Category> getCategory(String htmlString) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
        XMLEventReader reader = null;
        List<dto.Category> listCategory = new ArrayList<>();
        try {
            reader = factory.createXMLEventReader(new ByteArrayInputStream(htmlString.getBytes(StandardCharsets.UTF_8)));
            boolean inCategoryTag = false;
            boolean inAncestorTag = false;
            boolean inList = false;
            String link = "";
            String title = "";
            dto.Category tmpcate = null;
            while (reader.hasNext()) {

                XMLEvent eve = reader.nextEvent();

                if (eve.isStartElement()) {
                    StartElement ele = (StartElement) eve;

                    if (ele.getName().toString().equals("div")) {
                        Attribute sourceatt = ele.getAttributeByName(new QName("id"));
                        if (sourceatt != null) {
                            if (sourceatt.getValue().contains("submenu")) {
                                inList = true;
                            }
                        }
                    }
                    if (ele.getName().toString().equals("strong") && inList) {
                        inAncestorTag = true;
                    }

                    if (ele.getName().toString().equals("a") && inList) {
                        inCategoryTag = true;
                        Attribute sourceatt = ele.getAttributeByName(new QName("href"));
                        if (sourceatt != null) {
                            link = sourceatt.getValue();
                        }
                    }
                }
//                if (eve.isCharacters() && inAncestorTag) {
//                    Characters chars = (Characters) eve;
//                    if (chars.getData().matches("[^<>].*?")) {
//                        dto.Category cate = new dto.Category();
//                        cate.setName(title);
//                        cate.setAncestor(inAncestorTag);
//                        listCategory.add(cate);
//                    }
//                }

                if (eve.isCharacters() && (inCategoryTag || inAncestorTag) && inList) {

                    Characters chars = (Characters) eve;
                    title = chars.getData();
                    if (chars.getData().matches("[^<>].*?") && inAncestorTag) {
                        System.out.println(title);
                        dto.Category cate = new dto.Category();
                        cate.setName(title);
                        cate.setLink("https://dienmayxanh.com" + link);
                        listCategory.add(cate);

                        tmpcate = cate;
                    } else {
                        dto.Category cate = new dto.Category();
                        cate.setName(title);
                        cate.setLink("https://dienmayxanh.com" + link);
                        cate.setAncestorId(tmpcate);
                        listCategory.add(cate);

                    }

                }
                if (eve.isEndElement()) {
                    EndElement end = (EndElement) eve;
                    if (end.getName().toString().equals("a")) {
                        inCategoryTag = false;
                    }
                    if (end.getName().toString().equals("strong")) {
                        inAncestorTag = false;
                    }
                    if (end.getName().toString().equals("div")) {
                        inList = false;
                    }
                }

            }
            return listCategory;
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addCategoryToDB(List<dto.Category> listCategory) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session currentSession = factory.openSession();
        Transaction trans = currentSession.beginTransaction();
        listCategory.forEach(b -> {
            currentSession.save(b);
            if (listCategory.indexOf(b) % 20 == 0) {
                currentSession.flush();
                currentSession.clear();
            }
        });
        trans.commit();

        currentSession.close();

    }

    public static List<dto.Product> parseProduct(dto.Category cate) {
        InputStream is = null;
        try {
            URL url = new URL(cate.getLink());
            URLConnection yc = url.openConnection();

            yc.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Window NT 5.0)");

            is = yc.getInputStream();

            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String processString = ReadBigStringIn(in);
            Pattern pattern = Pattern.compile("<ul id=product-list.*?>.*?<\\/ul>");
            Matcher matcher = pattern.matcher(processString);

            is.close();
            if (matcher.find()) {
                System.out.println("found");
                processString = matcher.group()
                        .replaceAll("width=[0-9]{2}\\sheight=[0-9]{2}\\sdata-id=[0-9]{5,}", "")
                        .replaceAll("=([^\">]*?)\\s", "=\"$1\"" + " ")
                        .replaceAll("=([^\"]*?)>", "=\"$1\">")
                        .replaceAll("<p>(.*?)</div>", "</div>")
                        .replaceAll("<li(.*?)>", "<li$1/>")
                        .replaceAll("<img(.*?[^/])>", "<img$1/>")
                        .replaceAll("<p class=\"comp\">", "");
                return getProduct(processString, cate);
            } else {
                pattern = Pattern.compile("<ul class=\"cate\">.*(.*\\n)*?.*<\\/ul>");
                matcher = pattern.matcher(processString);
                if (matcher.find()) {
                    System.out.println("AWESOME with method 2");
                    processString = matcher.group()
                            .replaceAll("=([^\">]*?)\\s", "=\"$1\"" + " ")
                            .replaceAll("=([^\"]*?)>", "=\"$1\">")
                            .replaceAll("<img(.*?[^/])>", "<img$1/>");
                    return getProductinCate(processString, cate);
                } else {
                    System.out.println("try with method 3");
                    pattern = Pattern.compile("<ul id=\"product-list\".*?>.*(.*\\n)*?.*<\\/ul>");
                    matcher = pattern.matcher(processString);
                    if (matcher.find()) {
                        System.out.println("REALLY with method 3");
                        processString = matcher.group()
                                .replaceAll("=([^\">x]*?)\\s", "=\"$1\"" + " ")
                                .replaceAll("=([^\"]*?)>", "=\"$1\">")
                                .replaceAll("<img(.*?[^/])>", "<img$1/>");

                        return getProductinCate(processString, cate);
                    } else {
                        System.out.println("HAIZ -> try the best but NOT FOUND");
                    }
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
        return null;
    }

    public static List<dto.Product> getProduct(String htmlString, dto.Category cate) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
        XMLEventReader reader = null;
        List<dto.Product> listProduct = new ArrayList<>();
        try {
            reader = factory.createXMLEventReader(new ByteArrayInputStream(htmlString.getBytes(StandardCharsets.UTF_8)));
            boolean inProductTag = false;
            boolean inPriceTag = false;
            boolean inImageTag = false;
            String link = "";
            String title = "";
            String imgLink = "";
            String price = "";
            while (reader.hasNext()) {

                XMLEvent eve = reader.nextEvent();

                if (eve.isStartElement()) {
                    StartElement ele = (StartElement) eve;

                    if (ele.getName().toString().equals("li")) {
                        Attribute attr = ele.getAttributeByName(new QName("class"));
                        if (attr != null) {
                            if (attr.getValue().contains("pro")) {
                                inProductTag = true;
                            }
                        }
                    }

                    if (ele.getName().toString().equals("a") && inProductTag) {
                        Attribute attr = ele.getAttributeByName(new QName("href"));
                        if (attr != null) {
                            link = attr.getValue();
                        }
                        Attribute nameAttr = ele.getAttributeByName(new QName("title"));
                        if (nameAttr != null) {
                            title = nameAttr.getValue();
                        }
                    }
                    if (ele.getName().toString().equals("div")) {
                        Attribute attrb = ele.getAttributeByName(new QName("class"));
                        if (attrb != null) {
                            if (attrb.getValue().equals("thumbnail-product")) {
                                inImageTag = true;
                            }
                        }
                    }
                    if (ele.getName().toString().equals("img") && inProductTag && inImageTag) {
                        Attribute attr = ele.getAttributeByName(new QName("src"));
                        if (attr != null) {
                            imgLink = attr.getValue();
                            inImageTag = false;
                        }
                    }

                    if (ele.getName().toString().equals("strong") && inProductTag) {
                        inPriceTag = true;
                    }
                }
                if (eve.isCharacters() && inProductTag && inPriceTag) {
                    Characters chars = (Characters) eve;
                    price = chars.getData();
                    dto.Product product = new dto.Product();
                    product.setName(title);
                    product.setLink(link);
                    product.setImageLink(imgLink);
                    product.setDescription("");
                    product.setCategoryId(cate);
                    product.setPrice(new BigDecimal(price.replaceAll("\\.", "").replaceAll("[^0-9]*([0-9]*)[^0-9]*", "$1")));
                    product.setLink("https://dienmayxanh.com" + link);
                    inProductTag = false;
                    inImageTag = false;
                    inPriceTag = false;
                    listProduct.add(product);

                }
//                if (eve.isEndElement()) {
//                    EndElement ele = (EndElement) eve;
//                    if (ele.getName().toString().equals("ul")) {
//                        System.out.println("end ul");
//                        inCategoryTag = false;
//                    }
//                }
            }
            return listProduct;
        } catch (XMLStreamException e) {
            Writer writer = null;
            try {
                System.out.println(e.getMessage());
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("src/java/document/debug.html"), "UTF-8"));
                writer.write(htmlString);
                writer.close();
                System.exit(0);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public static List<dto.Product> getProductinCate(String htmlString, dto.Category cate) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
        XMLEventReader reader = null;
        List<dto.Product> listProduct = new ArrayList<>();
        try {
            reader = factory.createXMLEventReader(new ByteArrayInputStream(htmlString.getBytes(StandardCharsets.UTF_8)));
            boolean inProductTag = false;
            boolean inPriceTag = false;
            String link = "";
            String title = "";
            String imgLink = "";
            String price = "";
            while (reader.hasNext()) {

                XMLEvent eve = reader.nextEvent();

                if (eve.isStartElement()) {
                    StartElement ele = (StartElement) eve;

                    if (ele.getName().toString().equals("li")) {
                        inProductTag = true;
                    }

                    if (ele.getName().toString().equals("a") && inProductTag) {
                        Attribute attr = ele.getAttributeByName(new QName("href"));
                        if (attr != null) {
                            link = attr.getValue();
                        }

                    }
                    if (ele.getName().toString().equals("img") && inProductTag) {
                        Attribute attr = ele.getAttributeByName(new QName("src"));
                        if (attr != null) {
                            imgLink = attr.getValue();
                        }
                        Attribute nameAttr = ele.getAttributeByName(new QName("alt"));
                        if (nameAttr != null) {
                            title = nameAttr.getValue();
                        }
                    }
                    if (ele.getName().toString().equals("strong") && inProductTag) {
                        inPriceTag = true;
                    }
                }
                if (eve.isCharacters() && inProductTag && inPriceTag) {
                    Characters chars = (Characters) eve;
                    price = chars.getData();
                    dto.Product product = new dto.Product();
                    product.setName(title);
                    product.setLink(link);
                    product.setImageLink(imgLink);
                    product.setDescription("");
                    product.setCategoryId(cate);
                    product.setPrice(new BigDecimal(price.replaceAll("\\.", "").replaceAll("[^0-9]*([0-9]*)[^0-9]*", "$1")));
                    product.setLink("https://dienmayxanh.com" + link);
                    inProductTag = false;
                    inPriceTag = false;
                    listProduct.add(product);

                }
//                if (eve.isEndElement()) {
//                    EndElement ele = (EndElement) eve;
//                    if (ele.getName().toString().equals("ul")) {
//                        System.out.println("end ul");
//                        inCategoryTag = false;
//                    }
//                }
            }
            return listProduct;
        } catch (XMLStreamException e) {
            Writer writer = null;
            try {
                System.out.println(e.getMessage());
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("src/java/document/debug.html"), "UTF-8"));
                writer.write(htmlString);
                writer.close();
                System.exit(0);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public static void addProductToDB(List<dto.Product> listProduct) {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session currentSession = factory.openSession();
        Transaction trans = null;
        try {
            trans = currentSession.beginTransaction();
            listProduct.forEach(b -> {
                currentSession.save(b);
                if (listProduct.indexOf(b) % 20 == 0) {
                    currentSession.flush();
                    currentSession.clear();
                }
            });
            trans.commit();
            currentSession.close();

        } catch (Exception e) {
            trans.rollback();
            e.printStackTrace();
        } finally {

        }

    }

    public static String marshallToString(Store store) {
        try {
            JAXBContext jaxb = JAXBContext.newInstance(Store.class);
            Marshaller mar = jaxb.createMarshaller();
            StringWriter sw = new StringWriter();
            mar.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            mar.marshal(store, sw);
            return sw.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
//        public static void saveProductToXML(String xmlDataFilePath,  listMedicine) {
//        try {
//            JAXBContext ct = JAXBContext.newInstance(listMedicine.getClass());
//            Marshaller ms = ct.createMarshaller();
//            ms.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
//            ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//            ms.marshal(listMedicine, new File(xmlDataFilePath));
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//    }
//    public static void validateXMLBeforeSaveToDatabase(String xmlFilePath, ListMedicine listMedicine) {
//        try {
//            JAXBContext ct = JAXBContext.newInstance(listMedicine.getClass());
//            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            Schema schema = sf.newSchema(new File("src/sample/container/medicineData.xsd"));
//            Validator validator = schema.newValidator();
//            InputSource inputFile = new InputSource(
//                    new BufferedReader(new FileReader(xmlFilePath)));
//            validator.validate(new SAXSource(inputFile));
//            //saveToDatabase(listMedicine);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
