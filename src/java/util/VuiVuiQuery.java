/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import connectDB.HibernateUtil;
import dto.Category;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import static util.XMLUtil.ReadBigStringIn;

/**
 *
 * @author khai
 */
public class VuiVuiQuery implements Serializable {

    List<dto.Category> listCategory = null;
    List<dto.Product> listProduct = null;

    private void getProductFromOneCategory(String htmlString, Category cate) {
        if (htmlString == null) {
            return;
        }
        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
        XMLEventReader reader = null;

        try {
            reader = factory.createXMLEventReader(new ByteArrayInputStream(htmlString.getBytes(StandardCharsets.UTF_8)));
            boolean inProductTag = false;
            boolean inPriceTag = false;
            boolean inTitleTag = false;
            String link = "";
            String title = "";
            String imgLink = "";
            String price = "";
            while (reader.hasNext()) {

                XMLEvent eve = reader.nextEvent();

                if (eve.isStartElement()) {
                    StartElement ele = (StartElement) eve;

                    if (ele.getName().toString().equals("div")) {
                        Attribute attr = ele.getAttributeByName(new QName("class"));
                        if (attr != null) {
                            if (attr.getValue().contains("itmpro")) {
                                inProductTag = true;
                            }
                        }
                    }

                    if (ele.getName().toString().equals("a") && inProductTag) {
                        Attribute attr = ele.getAttributeByName(new QName("href"));
                        if (attr != null) {
                            link = attr.getValue();
                        }

                    }
                    if (ele.getName().toString().equals("h3") && inProductTag) {
                        inTitleTag = true;
                    }
                    if (ele.getName().toString().equals("img") && inProductTag) {
                        Attribute attr = ele.getAttributeByName(new QName("data-src"));
                        if (attr != null) {
                            imgLink = attr.getValue();
                        }
                    }
                    if (ele.getName().toString().equals("div") && inProductTag) {
                        Attribute attr = ele.getAttributeByName(new QName("class"));
                        if (attr != null) {
                            if (attr.getValue().contains("pricenew")) {
                                inPriceTag = true;
                            }
                        }
                    }
                }
                if (eve.isCharacters() && inTitleTag && inProductTag) {
                    Characters chars = (Characters) eve;
                    title = chars.getData();
                    inTitleTag = false;
                }
                if (eve.isCharacters() && inProductTag && inPriceTag) {
                    Characters chars = (Characters) eve;
                    price = chars.getData();
                    dto.Product product = new dto.Product();
                    product.setName(title);
                    product.setLink(link);
                    product.setImageLink(imgLink);
                    product.setDescription("");
                    product.setCategoryId(cate);
                    product.setPrice(new BigDecimal(price.replaceAll("\\.", "").replaceAll("[^0-9]*([0-9]*)[^0-9]*", "$1")));
                    product.setLink("https://vuivui.com" + link);
                    inProductTag = false;
                    inPriceTag = false;
                    inTitleTag = false;
                    if (listProduct == null) {
                        listProduct = new ArrayList<>();
                    }
                    listProduct.add(product);

                }
            }
        } catch (XMLStreamException e) {
            System.out.println(htmlString);
            System.out.println(e.getMessage());
        }
    }

    private List<String> getCategoryLink(String uri) {
        InputStream is = null;
        String processString = "";
        try {
            URL url = new URL(uri);
            URLConnection yc = url.openConnection();

            yc.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Window NT 5.0)");
            is = yc.getInputStream();

            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            processString = ReadBigStringIn(in);
            Pattern pattern = Pattern.compile("<nav.*>.*<\\/nav>");
            Matcher matcher = pattern.matcher(processString);
            if (matcher.find()) {
                System.out.println("found");
                processString = matcher.group();
                String tmpString = XMLUtil.changeUnicode(processString)
                        .replaceAll("=([^\"\\s]*?)>", "=\"$1\">")
                        .replaceAll("=([^\"]*?)\\s", "=\"$1\"" + " ");
                getCategoryFromHTML(tmpString, null, null);

//                writer = new BufferedWriter(new OutputStreamWriter(
//                        new FileOutputStream("src/document/debugVuiVui.html"), "UTF-8"));
//                writer.write(matcher.group());
//                writer.close();
                pattern = Pattern.compile("data-id=([0-9]{4})");
                matcher = pattern.matcher(processString);
                List<String> dataID = new ArrayList<>();
                boolean flag = false;
                while ((flag = matcher.find())) {
                    dataID.add(matcher.group().replaceAll("data-id=([0-9]{4})", "$1"));
                }
                return dataID;
            } else {
                System.out.println("not found -> cannot get category vuivui ");
            }

            is.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
        return null;
    }

    private void parseCategoryLink(List<String> cateID) {
        String childCateLink = "https://www.vuivui.com/aj/Home/LoadListChildCate?parentId=";
        List<Category> tmpList = listCategory;
        cateID.forEach(linkID -> {
            if (linkID != null) {
                try {
                    InputStream is = null;
                    URL url = new URL(childCateLink + linkID);
                    System.out.println(url.toString());
                    URLConnection yc = url.openConnection();

                    yc.addRequestProperty("User-Agent",
                            "Mozilla/4.0 (compatible; MSIE 6.0; Window NT 5.0)");
                    is = yc.getInputStream();
                    BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    String processString = ReadBigStringIn(in);
                    processString = XMLUtil.changeHTMLSpecialText(processString);
                    Pattern pattern = Pattern.compile("(\\\\u[A-Za-z0-9]{4})");
                    Matcher matcher = pattern.matcher(processString);
                    StringBuffer bufStr = new StringBuffer();
                    boolean flag = false;
                    while ((flag = matcher.find())) {
                        String rep = matcher.group();
                        matcher.appendReplacement(bufStr, Character.toString((char) Integer.parseInt(rep.substring(2), 16)));
                    }
                    matcher.appendTail(bufStr);
                    String result = bufStr.toString();
                    matcher = pattern.compile("<a.*?>.*</a>").matcher(result);
                    if (matcher.find()) {
//                    System.out.println(matcher.group());
                        result = matcher.group()
                                .replaceAll("\\/\\/", "")
                                .replaceAll("<img(.*?)>", "")
                                .replaceAll("=([^\"]*?)>", "=\"$1\">");
//                    System.out.println(XMLUtil.changeUnicode(result));

                        getCategoryFromHTML("<root>" + XMLUtil.changeUnicode(result) + "</root>", tmpList, linkID);

                    }

                } catch (MalformedURLException ex) {
                    Logger.getLogger(VuiVuiQuery.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(VuiVuiQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }

    public List<dto.Category> getCategory(String uri) {
        parseCategoryLink(getCategoryLink(uri));
        System.out.println("Contain: " + listCategory.size() + " categories");
        return listCategory;
    }

    public List<dto.Product> getAllProduct(List<dto.Category> listCategory) {
        listCategory.forEach(cate -> {
            getProductFromOneCategory(parseProduct(cate), cate);
        });
        System.out.println("Contain: " + listProduct.size() + " products");

        return listProduct;
    }

    public void insertCategoryToDB(List<dto.Category> listCategory) {
        if (listCategory != null) {
            XMLUtil.addCategoryToDB(listCategory);
        }
    }

    public void insertProductToDB(List<dto.Product> listProduct) {
        if (listProduct != null) {
            XMLUtil.addProductToDB(listProduct);
        }
    }

    public List<Category> getParentCateory() {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session currentSession = factory.openSession();
        String hql = "FROM Category C WHERE C.link in :cateLink";
        Query query = currentSession.createQuery(hql);
        query.setParameter("cateLink", "vuivui");
        List<Category> file = query.list();

        currentSession.close();
        return file;
    }

    private void getCategoryFromHTML(String htmlString, List<Category> parentList, String linkID) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
        XMLEventReader reader = null;
        Category parrentCate = null;
        if (parentList != null && linkID != null) {
            parrentCate = parentList.stream().filter(cate -> cate.getRealIDofParent().contains(linkID)).findFirst().get();
        }
        try {
            reader = factory.createXMLEventReader(new InputStreamReader(new ByteArrayInputStream(htmlString.getBytes("UTF-8")), "UTF-8"));
            boolean inCategoryTag = false;
            String link = "";
            String title = "";
            String realID = "";

            while (reader.hasNext()) {

                XMLEvent eve = reader.nextEvent();

                if (eve.isStartElement()) {
                    StartElement ele = (StartElement) eve;

                    if (ele.getName().toString().equals("a")) {
                        Attribute sourceatt = ele.getAttributeByName(new QName("href"));
                        link = sourceatt.getValue();
                        inCategoryTag = true;
                        Attribute dataID = ele.getAttributeByName(new QName("data-id"));
                        if (dataID != null) {
                            realID = dataID.getValue();
                        }
                    }
                }
                if (eve.isCharacters() && inCategoryTag) {
                    Characters chars = (Characters) eve;
                    title = chars.getData();
                    dto.Category cate = new dto.Category();
                    cate.setName(title);
                    cate.setLink(link);
                    if (link.contains("vuivui.com")) {
                        cate.setLink("https://" + link);

                    } else {
                        cate.setRealIDofParent(realID);
                        cate.setLink("https://vuivui.com" + link);
                    }
                    if (parrentCate != null) {
                        cate.setAncestorId(parrentCate);
                    }

                    inCategoryTag = false;
                    if (listCategory == null) {
                        listCategory = new ArrayList<>();
                    }
                    listCategory.add(cate);

                }

            }
        } catch (XMLStreamException e) {
            System.out.println(e.getMessage());
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String parseProduct(dto.Category cate) {
        InputStream is = null;
        try {
            URL url = new URL(cate.getLink());
            URLConnection yc = url.openConnection();
            System.out.println(cate.getLink());
            yc.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Window NT 5.0)");

            is = yc.getInputStream();

            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String processString = ReadBigStringIn(in);
            Pattern pattern = Pattern.compile("<div id=grid-product.*?>.*?<div class=pagination>");
            Matcher matcher = pattern.matcher(processString);

            is.close();
            if (matcher.find()) {
                System.out.println("found");
                //System.out.println(matcher.group());
                processString = matcher.group()
                        .replaceAll("<div class=pagination>", "")
                        .replaceAll("=([^\">]*?)\\s", "=\"$1\"" + " ")
                        .replaceAll("=([^\"]*?)>", "=\"$1\">")
                        .replaceAll("<span.*?>.*?</span>", "")
                        .replaceAll("<div class=\"pr\">.*?</div>", "")
                        .replaceAll("<img(.*?[^/])>", "<img$1/>");
                processString = XMLUtil.changeUnicode(processString);
                //System.out.println(processString);
                return processString.concat("</div>");
            } else {
                System.out.println("not found");
                //System.out.println(processString);

                return null;
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex1) {
                Logger.getLogger(VuiVuiQuery.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(XMLUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return null;
    }
}
