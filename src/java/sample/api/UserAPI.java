/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.api;

/**
 *
 * @author khai
 */
import dao.CustomerDAO;
import dto.Customer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("user")
public class UserAPI {

    @Context
    HttpServletRequest request;
    @Context
    HttpServletResponse response;

    @GET
    @Path("/logout")

    public void checkRequestServlet(@PathParam("username") String username) {
        try {
            String usernameInServer = (String) request.getSession().getAttribute("USERNAME");
            if (usernameInServer != null) {
                request.getSession().removeAttribute("USERNAME");
            }
            response.sendRedirect("/");
        } catch (IOException ex) {
            Logger.getLogger(UserAPI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @POST
    @Path("/register")
    @Consumes("application/x-www-form-urlencoded")
    public Response register(@FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("email") String email,
            @FormParam("fullname") String fullname
    ) {

        Customer customer = new Customer();
        customer.setEmail(email);
        customer.setFullname(fullname);
        customer.setPassword(password);
        customer.setUsername(username);
        CustomerDAO dao = new CustomerDAO();
        boolean checkExist = false;
        checkExist = dao.getAllCustomer().stream()
                .anyMatch(cus -> cus.getUsername().equalsIgnoreCase(username));
        if (!checkExist) {
            dao.addCustomer(customer);
            return Response.status(200).entity("Add successful").build();
        } else {
            return Response.status(Response.Status.CONFLICT).entity("Duplicate username").build();
        }

    }

    @POST
    @Path("/login")
    @Consumes("application/x-www-form-urlencoded")
    public Response login(@FormParam("username") String username,
            @FormParam("password") String password) {
        CustomerDAO dao = new CustomerDAO();
        boolean checkLogin = false;
        checkLogin = dao.getAllCustomer().stream().filter(cus -> cus.getUsername().equalsIgnoreCase(username))
                .anyMatch(cus -> cus.getPassword().equals(password));
        if (checkLogin) {
            return Response.status(200).entity("LOGIN").build();
        } else {
            return Response.status(200).entity("DENY").build();
        }
    }

    @GET
    @Path("/hello")
    public Response putMessage() {
        String data = "Message Saved";
        return Response.status(200).entity(data).build();
    }
}
