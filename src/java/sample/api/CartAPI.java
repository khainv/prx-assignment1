/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.api;

import dao.CustomerDAO;
import dto.Category;
import dto.Product;
import dto.Store;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author khai
 */
@Path("cart")
public class CartAPI {

    @Context
    HttpServletRequest request;
    @Context
    HttpServletResponse response;

    @POST
    @Path("/addToCart")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addToCart(String s) {
        try {
            String cartXML = s;
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(cartXML));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("product");
            Map<String, String> cartList = new HashMap<String, String>();
            for (int i = 0; i < nodes.getLength(); i++) {
                Element line = (Element) nodes.item(i);
                String id = line.getAttribute("id");
                String qty = line.getAttribute("qty");
                if (id != null && qty != null) {
                    cartList.put(id, qty);
                }
            }
            Map<Product, String> realCart = new HashMap<>();
//            Category tmpCategory = new Category();
//            Store tmpStore = new Store();
            ServletContext application = request.getServletContext();
            Store store = (Store) application.getAttribute("STORE");
            store.getCategorys().forEach(parentCate -> {
                parentCate.getSubCategory().forEach(cate -> {
                    cartList.forEach((id, qty) -> {
                        List<Product> result = cate.getProductList().stream()
                                .filter(product -> product.getId() == Integer.parseInt(id))
                                .collect(Collectors.toList());
                        if (result != null) {
                            if (result.size() > 0) {
                                result.forEach(r -> {
                                    r.setPrice(r.getPrice().stripTrailingZeros());
                                    realCart.put(r, cartList.get(id));

                                });
                            }
                        }
                    });
                });
            });

            //tmpStore.getCategorys().add(tmpCategory);
            request.getSession().setAttribute("CART", realCart);
            return Response.status(200).entity("RECEIVED CART").entity(s).build();
        } catch (SAXException ex) {
            Response.serverError().entity(ex).entity(s).build();
        } catch (IOException ex) {
            Response.serverError().entity(ex).entity(s).build();

        } catch (ParserConfigurationException ex) {
            Response.serverError().entity(ex).entity(s).build();

        }
        return Response.serverError().build();
    }
}
