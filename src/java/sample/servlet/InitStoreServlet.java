/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.servlet;

import dao.CategoryDAO;
import dao.ProductDAO;
import dto.Category;
import dto.Product;
import dto.Store;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Crawler;
import util.XMLUtil;

/**
 *
 * @author khai
 */
@WebServlet(name = "InitStoreServlet", urlPatterns = {"/InitStoreServlet"})
public class InitStoreServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.addHeader("Access-Control-Allow-Origin", "*");
        try {
            /* TODO output your page here. You may use following sample code. */
            String forwardLink = "showProductList.jsp";
            initStore();
            if (request.getAttribute("REQUEST_UPDATE_STORE") != null) {
                forwardLink = "ResponseStoreServlet";
            }

            RequestDispatcher rd = request.getRequestDispatcher(forwardLink);
            rd.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initStore() throws FileNotFoundException, IOException {
        ServletContext application = getServletConfig().getServletContext();

        if (application.getAttribute("INFO") == null) {
            callUpdateStore();
        }
    }

    private void callUpdateStore() throws UnsupportedEncodingException, FileNotFoundException, IOException {
        ServletContext application = getServletConfig().getServletContext();
        Crawler cr = new Crawler();
        cr.getDMX();
        cr.getVuiVui();
        CategoryDAO cateDAO = new CategoryDAO();
        List<Category> cateList = cateDAO.getAllCategorys();
        ProductDAO productDAO = new ProductDAO();
        List<Product> productList = productDAO.getAllProducts();

        Store store = new Store();
        List<Category> parentCategoryList = cateList.stream().filter(cate -> cate.getAncestorId() == null).collect(Collectors.toList());
        parentCategoryList.forEach(parentCate -> {
            parentCate.setName(parentCate.getName().toUpperCase());
            List<Category> tmpList = cateList.stream()
                    .filter(cate -> cate.getAncestorId() != null)
                    .filter(cate -> parentCate.getId().equals(cate.getAncestorId().getId()))
                    .collect(Collectors.toList());

            tmpList.forEach(cate -> {

                List<Product> tmpProductList = productList.stream()
                        .filter(product -> product.getCategoryId().getId().equals(cate.getId()))
                        .collect(Collectors.toList());

                cate.getProductList().addAll(tmpProductList);
                if (cate.getAncestorId() != null) {
                    cate.setParentID(cate.getAncestorId().getId());
                    cate.setParentName(cate.getAncestorId().getName());
                }

            });

            parentCate.getSubCategory().addAll(tmpList);
        });

        store.getCategorys().addAll(parentCategoryList);

        String str = XMLUtil.marshallToString(store);
        application.setAttribute("STORE", store);
        String webInfPath = getServletConfig().getServletContext().getRealPath("WEB-INF");
//        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(webInfPath + "/store.xml"), "UTF-8"));
//        writer.write(str);
//        writer.close();
        BufferedReader br = new BufferedReader(new FileReader(webInfPath + "/NavLeft.xsl"));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        application.setAttribute("NAVLEFT_XSL", sb.toString());
        application.setAttribute("INFO", str);
        System.out.println("Init INFO complete");

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
