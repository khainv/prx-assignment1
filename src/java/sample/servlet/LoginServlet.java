/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.servlet;

import dao.CustomerDAO;
import dto.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

/**
 *
 * @author khai
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String forwardLink = "showProductList.jsp";
            if (request.getServletContext().getAttribute("INFO") == null) {
                forwardLink = "index.html";
            }
            if (request.getServletContext().getAttribute("USERNAME") == null) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                CustomerDAO dao = new CustomerDAO();
                Customer customer = null;
                customer = dao.getAllCustomer().stream()
                        .filter(cus -> cus.getUsername().equalsIgnoreCase(username))
                        .filter(cus -> cus.getPassword().equals(password)).findFirst().orElse(null);
                if (customer != null) {
                    request.getSession().setAttribute("USERNAME", customer.getUsername());
                    request.getSession().setAttribute("FULLNAME", customer.getFullname());
                    request.getSession().setAttribute("CURRENT_USER", customer);

                } else {
                    forwardLink = "login.jsp";
                    request.setAttribute("INVALID_ACCOUNT", true);
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(forwardLink);
            rd.forward(request, response);

        } catch (Exception ex) {
            response.getWriter().write(ex.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
