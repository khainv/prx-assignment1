/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.servlet;

import dto.Category;
import dto.Product;
import dto.Store;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.XMLUtil;

/**
 *
 * @author khai
 */
@WebServlet(name = "ProductDetailServlet", urlPatterns = {"/ProductDetailServlet"})
public class ProductDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String productID = request.getParameter("productID");
            String forwardLink = "productDetail.jsp";
            if (productID != null) {
                ServletContext application = getServletConfig().getServletContext();
                Store store = (Store) application.getAttribute("STORE");
                if (store == null) {
                    request.setAttribute("REQUEST_UPDATE_STORE", true);
                    request.setAttribute("SERVLET_CALLED_NAME", this.getClass().getName());
                    request.setAttribute("PRODUCT_ID", productID);
                    forwardLink = "InitStoreServlet";

                } else {
                    Category tmpCategory = new Category();
                    Store tmpStore = new Store();
                    store.getCategorys().forEach(parentCate -> {
                        parentCate.getSubCategory().forEach(cate -> {
                            Product result = cate.getProductList().stream().filter(product -> product.getId() == Integer.valueOf(productID)).findFirst().orElse(null);
                            if (result!=null) {
                                result.setPrice(result.getPrice().stripTrailingZeros());
                                tmpCategory.getProductList().add(result);
                                tmpStore.getCategorys().add(tmpCategory);
                                request.setAttribute("productDetail", result);
                                
                            }
                        });
                    });
                    String str = XMLUtil.marshallToString(tmpStore);
                    String webInfPath = getServletConfig().getServletContext().getRealPath("WEB-INF");
                    Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(webInfPath + "/productDetail.xml"), "UTF-8"));
                    writer.write(str);
                    writer.close();
                    forwardLink = "productDetail.jsp";
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(forwardLink);
            rd.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
