/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.servlet;

import connectDB.HibernateUtil;
import dto.Store;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author khai
 */
@WebServlet(name = "ResponseStoreServlet", urlPatterns = {"/ResponseStoreServlet"})
public class ResponseStoreServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml");
        response.setCharacterEncoding("UTF-8");
        try {
            response.addHeader("Access-Control-Allow-Origin", "*");
            if (getServletConfig().getServletContext().getAttribute("INFO") == null) {
                RequestDispatcher rd = request.getRequestDispatcher("InitStoreServlet");
                request.setAttribute("REQUEST_UPDATE_STORE", true);
                rd.forward(request, response);
            } else {
                response.getWriter().write((String) getServletConfig().getServletContext().getAttribute("INFO"));
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void marshallerProductToTransfer(Store o, OutputStream os) {
        try {
            JAXBContext jaxb = JAXBContext.newInstance(o.getClass());
            Marshaller marshal = jaxb.createMarshaller();
            marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshal.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshal.marshal(o, os);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private List getList(String hql) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(hql);
        return query.list();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
