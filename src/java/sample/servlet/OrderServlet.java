/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.servlet;

import dao.OrderDAO;
import dao.OrderDetailDAO;
import dto.Customer;
import dto.Order;
import dto.OrderDetail;
import dto.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khai
 */
@WebServlet(name = "OrderServlet", urlPatterns = {"/OrderServlet"})
public class OrderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String forwardLink = "login.jsp";
            OrderDAO dao = new OrderDAO();
            Customer user = (Customer) request.getSession().getAttribute("CURRENT_USER");
            if (user != null) {
                List<Order> orderlistTMP = dao.getAllOrder().stream().filter(o -> o.getCustomers().getCustomerId()==user.getCustomerId()).collect(Collectors.toList());
                Map<Order, Integer> orderlist = new HashMap();
                OrderDetailDAO odDAO = new OrderDetailDAO();
                List<OrderDetail> listOD = odDAO.getAllOrderDetail();
                
                
                orderlistTMP.forEach(order -> {
                    List<OrderDetail> tmpOD = listOD.stream().filter(od -> od.getOrder().getOrderId().equalsIgnoreCase(order.getOrderId())).collect(Collectors.toList());
                    
                    tmpOD.forEach(od -> {
                        int price = od.getPrice().multiply(new BigDecimal(od.getQuantity())).intValue();
                        orderlist.put((Order) order, price);
                    });
                    
                });
                request.getSession().setAttribute("ORDERLIST", orderlist);
                forwardLink = "order.jsp";
            }
            RequestDispatcher rd = request.getRequestDispatcher(forwardLink);
            rd.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
